#include <string.h>
#include <stdlib.h>
#include <time.h>
#include "cmsis_os.h"
#include "usart.h"
#include "led.h"
#include "can.h"
#include "common.h"
#include "usart_task.h"
#include "can_task.h"
#include "normal_task.h"
#include "sensor_task.h"
#include "can_send_task.h"
#include "wwdg.h"
#include "w25q16.h"

osThreadId defaultTaskHandle;
static Global_Inform_ global_inform;
void StartDefaultTask(void const * argument);

int main(void)
{
  HAL_Init();
  SystemClock_Config();
	NVIC_SetPriorityGrouping(NVIC_PriorityGrop_0);
	Led_Init();
	Usart_Init(&global_inform);
	can_init(&global_inform);
	w25qxx_Init();
	read_demarcate_value(&global_inform);
	printf("boardid:0x%x\n\r",global_inform.demarcate_inform.board_id);
	printf("can:0x%x\n\r",global_inform.demarcate_inform.can_id);
	printf("concentrate_can_id:0x%x\n\r",global_inform.demarcate_inform.concentrate_can_id);
	for(unsigned int i = 0; i < 12 ; i++){
		printf("port encode:%.*s\n\r", 40, global_inform.demarcate_inform.port_encode[i]);
	}
	
	iwdg_init();
	global_inform.dispatch_inform.xEventGroup = xEventGroupCreate();
  osThreadDef(defaultTask, StartDefaultTask, osPriorityIdle, 0, 128);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), &global_inform);
	xTaskCreate(usart_task, "usart_task", 64, &global_inform,osPriorityAboveNormal, NULL);
	xTaskCreate(can_task, "can_task", 64, &global_inform,osPriorityAboveNormal, NULL);
	xTaskCreate(normal_task, "normal_task", 256, &global_inform,osPriorityNormal, NULL);
	xTaskCreate(can_send_task, "can_send_task", 64, &global_inform,osPriorityAboveNormal, NULL);
	xTaskCreate(sensor_task, "sensor_task", 256, &global_inform,osPriorityNormal, NULL);
  osKernelStart();
	
	return 0;
}

/* StartDefaultTask function */
void StartDefaultTask(void const * argument)
{
  unsigned int count = 0;
	unsigned char index = 0;
	Global_Inform_* global_inform = (Global_Inform_ *)argument;
	unsigned int rand_ = global_inform->demarcate_inform.board_id;
	rand_ = rand_& 0xfff;
	srand(rand_); 
	unsigned short delay_ = rand() % 2000;  
	LED1(1); LED2(1);LED3(1);LED4(1);LED5(1);LED6(1);
	LED7(1); LED8(1);LED9(9);LED10(10);LED11(1);LED12(1);
	for(unsigned int i = 0; i < 12; i++)
	 global_inform->dispatch_inform.led_status[i]= 0;
	osDelay(1000);
	LED1(0); LED2(0);LED3(0); LED4(0);LED5(0); LED6(0);LED7(0); LED8(0);LED9(0); LED10(0); LED11(0);LED12(0);
	global_inform->TxMessage.Data[0] = CAN_BOARD_ONLINE_NOTIFY;
	global_inform->TxMessage.Data[1] = global_inform->demarcate_inform.board_id;
	Can_MsgSend(2);
	while(1){
		index = count % 12;
		switch(index){
			case 0: LED12(0); LED1(1);global_inform->dispatch_inform.led_status[11] = 0;global_inform->dispatch_inform.led_status[0]=1;break;
			case 1: LED1(0); LED2(1); global_inform->dispatch_inform.led_status[0]= 0;global_inform->dispatch_inform.led_status[1]=1;break;
		  case 2: LED2(0); LED3(1);global_inform->dispatch_inform.led_status[1] = 0;global_inform->dispatch_inform.led_status[2]=1;break;
			case 3: LED3(0); LED4(1);global_inform->dispatch_inform.led_status[2] = 0;global_inform->dispatch_inform.led_status[3]=1;break;
			case 4: LED4(0); LED5(1);global_inform->dispatch_inform.led_status[3] = 0;global_inform->dispatch_inform.led_status[4]=1;break;
			case 5: LED5(0); LED6(1);global_inform->dispatch_inform.led_status[4] = 0;global_inform->dispatch_inform.led_status[5]=1;break;
			case 6: LED6(0); LED7(1);global_inform->dispatch_inform.led_status[5] = 0;global_inform->dispatch_inform.led_status[6]=1;break;
			case 7: LED7(0); LED8(1);global_inform->dispatch_inform.led_status[6] = 0;global_inform->dispatch_inform.led_status[7]=1;break;
			case 8: LED8(0); LED9(1);global_inform->dispatch_inform.led_status[7] = 0;global_inform->dispatch_inform.led_status[8]=1;break;
			case 9: LED9(0); LED10(1);global_inform->dispatch_inform.led_status[8]= 0;global_inform->dispatch_inform.led_status[9]=1;break;
			case 10: LED10(0); LED11(1);global_inform->dispatch_inform.led_status[9] = 0;global_inform->dispatch_inform.led_status[10]=1;break;
			case 11: LED11(0); LED12(1);global_inform->dispatch_inform.led_status[10] = 0;global_inform->dispatch_inform.led_status[11]=1;break;		
		}
		count ++;
		osDelay(500);
		if((global_inform->dispatch_inform.concentrator_status == ONLINE) && (count > 12))
			break;
	}
	LED1(0); LED2(0);LED3(0);LED4(0);LED5(0);LED6(0);
	LED7(0); LED8(0);LED9(0);LED10(0);LED11(0);LED12(0);
	for(unsigned int i = 0; i < 12; i++)
		global_inform->dispatch_inform.led_status[i]= 0;
	
	//upload can id	
	global_inform->TxMessage.Data[0] = CAN_PORT_OUT_CANID0;
	global_inform->TxMessage.Data[1] = global_inform->demarcate_inform.board_id;
	Can_MsgSleepSend(2);
	
	osDelay(delay_);
	
	//upload port encode
	for(unsigned int i = 0; i < 12; i++){
		global_inform->TxMessage.Data[0] = CAN_PORT_OUT_ENCODE00;
		global_inform->TxMessage.Data[1] = i;
		for(int j = 0; j < 6; j++ ){
			global_inform->TxMessage.Data[j + 2] = global_inform->demarcate_inform.port_encode[i][j];
		}
		Can_MsgSleepSend(8);
	
		global_inform->TxMessage.Data[0] = CAN_PORT_OUT_ENCODE01;
		global_inform->TxMessage.Data[1] = i;
		for(int j = 0; j < 6; j++ ){
			global_inform->TxMessage.Data[j + 2] = global_inform->demarcate_inform.port_encode[i][j + 6];
		}
		Can_MsgSleepSend(8);
		
		global_inform->TxMessage.Data[0] = CAN_PORT_OUT_ENCODE02;
		global_inform->TxMessage.Data[1] = i;
		for(int j = 0; j < 6; j++ ){
			global_inform->TxMessage.Data[j + 2] = global_inform->demarcate_inform.port_encode[i][j + 12];
		}
		Can_MsgSleepSend(8);

		global_inform->TxMessage.Data[0] = CAN_PORT_OUT_ENCODE03;
		global_inform->TxMessage.Data[1] = i;
		for(int j = 0; j < 6; j++ ){
			global_inform->TxMessage.Data[j + 2] = global_inform->demarcate_inform.port_encode[i][j + 18];
		}
		Can_MsgSleepSend(8);
		
		global_inform->TxMessage.Data[0] = CAN_PORT_OUT_ENCODE04;
		for(int j = 0; j < 6; j++ ){
			global_inform->TxMessage.Data[j + 2] = global_inform->demarcate_inform.port_encode[i][j + 24];
		}
		Can_MsgSleepSend(8);
		
		global_inform->TxMessage.Data[0] = CAN_PORT_OUT_ENCODE05;
		global_inform->TxMessage.Data[1] = i;
		for(int j = 0; j < 6; j++ ){
			global_inform->TxMessage.Data[j + 2] = global_inform->demarcate_inform.port_encode[i][j + 30];
		}
		Can_MsgSleepSend(8);
		
		global_inform->TxMessage.Data[0] = CAN_PORT_OUT_ENCODE06;
		global_inform->TxMessage.Data[1] = i;
		global_inform->TxMessage.Data[2] = global_inform->demarcate_inform.port_encode[i][36];
		global_inform->TxMessage.Data[3] = global_inform->demarcate_inform.port_encode[i][37];
		global_inform->TxMessage.Data[4] = global_inform->demarcate_inform.port_encode[i][38];
		global_inform->TxMessage.Data[5] = global_inform->demarcate_inform.port_encode[i][39];
		Can_MsgSleepSend(6);	
	}
	CallBackControlFunction(global_inform);
	vTaskDelete(defaultTaskHandle);
}


