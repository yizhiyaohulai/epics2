#include "spinorflash.h"
#include "stm32f4xx_hal.h"
#include "string.h"
#include "stdlib.h"

#define FLASHSIZE				0x1000000
#define FLASHPAGESIZE		0x100

#define DEMARCATE_BUF_SIZE       1023
#define SECTOR_0_START_ADDR      0x00000000
#define SECTOR_1_START_ADDR      0x00010000
#define SECTOR_2_START_ADDR      0x00020000
#define SECTOR_3_START_ADDR      0x00030000
#define SECTOR_4_START_ADDR      0x00040000

SPI_HandleTypeDef hspi1;

void spinorflash_init(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;
	
	__GPIOA_CLK_ENABLE();
	__SPI1_CLK_ENABLE();
	
  //HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7, GPIO_PIN_RESET);
	
	//CS	
	GPIO_InitStruct.Pin			=GPIO_PIN_4;
	GPIO_InitStruct.Mode		=GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Speed  	=GPIO_SPEED_FAST;
	GPIO_InitStruct.Pull 		=GPIO_PULLUP;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	
  hspi1.Instance 								= SPI1;
  hspi1.Init.Mode 							= SPI_MODE_MASTER;
  hspi1.Init.Direction 					= SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize 					= SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity 				= SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase 					= SPI_PHASE_1EDGE;
  hspi1.Init.NSS 								= SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler 	= SPI_BAUDRATEPRESCALER_2;
  hspi1.Init.FirstBit 					= SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode 						= SPI_TIMODE_DISABLED;
  hspi1.Init.CRCCalculation 		= SPI_CRCCALCULATION_DISABLED;
  hspi1.Init.CRCPolynomial 			= 7;
  HAL_SPI_Init(&hspi1);
	
	__HAL_SPI_ENABLE(&hspi1);
	
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_SET);	
}

void spi_norflash_exit(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;
	HAL_SPI_DeInit(&hspi1);
	__HAL_SPI_DISABLE(&hspi1);
	__SPI1_CLK_DISABLE();

	GPIO_InitStruct.Pin			=GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_7|GPIO_PIN_6;
	GPIO_InitStruct.Mode		=GPIO_MODE_ANALOG;
	GPIO_InitStruct.Speed  	=GPIO_SPEED_LOW;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
}


static int IsFlashBusy(void)
{
	uint8_t ucSR;
	unsigned char tx_data[1] ={SPI_FLASH_INS_RDSR} ;
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_RESET);
	HAL_SPI_Transmit(&hspi1, tx_data, 1, 100);
	HAL_SPI_Receive(&hspi1, &ucSR, 1, 100);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_SET);
	
	if (ucSR & SPI_FLASH_WIP)
		return 1;
	else 
		return 0;
}

/* Disable Write protection */
static void FlashWriteEnable(void)
{
	unsigned char tx_data[1] ={SPI_FLASH_INS_WREN} ;
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_RESET);
	HAL_SPI_Transmit(&hspi1, tx_data, 1, 100);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_SET);
}

static void FlashWaitWriteEnd(void)
{
	while(IsFlashBusy())
		;
}	

void SendFlashAddr(uint32_t ulAddr)
{
	/* 128Mb : 3-addr byte mode */
	unsigned char tx_data[3] ;
	tx_data[0] = (uint8_t)(ulAddr >> 16);
	tx_data[1] = (uint8_t)(ulAddr >> 8);
	tx_data[2] = (uint8_t)ulAddr;
	
	HAL_SPI_Transmit(&hspi1, tx_data, 3, 100);
}

int8_t FlashPageProgram(uint32_t ulWriteAddr, uint8_t *p_ucArray, unsigned char ucNumByteToWrite)
{
	unsigned char tx_data[1] ={SPI_FLASH_INS_PP} ;
	if ((ulWriteAddr > (FLASHSIZE - FLASHPAGESIZE)) || (ucNumByteToWrite == 0))
		return 1;
	
	FlashWriteEnable();
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_RESET);
	HAL_SPI_Transmit(&hspi1, tx_data, 1, 100);
	SendFlashAddr(ulWriteAddr);
	HAL_SPI_Transmit(&hspi1, p_ucArray, ucNumByteToWrite, 100);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_SET);
	FlashWaitWriteEnd();
	
	return 0;
}

uint8_t FlashReadData(uint32_t ulReadAddr, uint8_t *p_ucArray, unsigned char ucNumByteToRead)
{
	unsigned char tx_data[1] ={SPI_FLASH_INS_READ} ;
	if ( ulReadAddr > FLASHSIZE)
		return 1;

	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_RESET);
	HAL_SPI_Transmit(&hspi1, tx_data, 1, 100);
	SendFlashAddr(ulReadAddr);
	
	HAL_SPI_Receive(&hspi1, p_ucArray, ucNumByteToRead, 100);
	
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_SET);
	
	return 0;
}

int8_t FlashSubSectorErase(uint32_t ulSubSectorAddr)
{
	unsigned char tx_data[1] = {SPI_FLASH_INS_SSE};
	if ( ulSubSectorAddr > (FLASHSIZE - FLASHPAGESIZE) )
		return 1;
	
	FlashWriteEnable();
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_RESET);
	HAL_SPI_Transmit(&hspi1, tx_data, 1, 100);
	SendFlashAddr(ulSubSectorAddr);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_SET);
	FlashWaitWriteEnd();
	
	return 0;
}

int8_t FlashSectorErase(uint32_t ulSubSectorAddr)
{
	unsigned char tx_data[1] = {SPI_FLASH_INS_SE};
	if ( ulSubSectorAddr > (FLASHSIZE - FLASHPAGESIZE) )
		return 1;
	
	FlashWriteEnable();
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_RESET);
	HAL_SPI_Transmit(&hspi1, tx_data, 1, 100);
	SendFlashAddr(ulSubSectorAddr);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_SET);
	FlashWaitWriteEnd();
	
	return 0;
}

void FlashEraseBulk(void)
{
  unsigned char tx_data[1] = {SPI_FLASH_INS_BE};
	FlashWriteEnable();
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_RESET);
	HAL_SPI_Transmit(&hspi1, tx_data, 1, 100);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_SET);
	FlashWaitWriteEnd();
}

uint32_t FlashReadDevId(void)
{
	unsigned char tx_data[1] = {SPI_FLASH_INS_RDID}; 
	unsigned char rx_data[3];
	unsigned int id = 0;
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_RESET);
	HAL_SPI_Transmit(&hspi1, tx_data, 1, 100);
	HAL_SPI_Receive(&hspi1, rx_data, 3, 100);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_SET);
	
	id = (rx_data[0] << 16) + (rx_data[1] << 8) + rx_data[2];
	return id;
}

//can id 
//0 canid 7
//1 canid 6
//2 canid 5
//3 canid 4
//4 canid 3
//5 canid 2
//6 canid 1
//7 canid 0

//concentrator can id 
//8 canid 7
//9 canid 6
//10 canid 5
//11 canid 4
//12 canid 3
//13 canid 2
//14 canid 1
//15 canid 0

//threshold_status
//(20-23) (24-27) (28-31) (32-35) (36-39) (40-43) (44-47) (48-51) (52-55) (56-59) (60-63) (64-67)

//board id 
//68-71


//passwd
// (90-97)

//already save passwd
//98

//port 0 edcode
//100 _ 
//port 1 edcode
//140 _ 
//port 2 edcode
//180 _ 
//port 3 edcode
//220 _ 
//port 4 edcode
//260 _ 
//port 5 edcode
//300 _ 
//port 6 edcode
//340 _ 
//port 7 edcode
//380 _ 
//port 8 edcode
//420_ 
//port 9 edcode
//460 _ 
//port 10 edcode
//500 _ 
//port 11 edcode
//540_ 579

unsigned int read_demarcate_value(Global_Inform_* global_inform){
	char buffer[1024];
	FlashReadData(SECTOR_0_START_ADDR, (unsigned char *)buffer, 255);
	FlashReadData(SECTOR_1_START_ADDR, (unsigned char *)(buffer + 256) , 255);
	FlashReadData(SECTOR_2_START_ADDR, (unsigned char *)(buffer + 512), 255);
	
	global_inform->demarcate_inform.can_id = (buffer[4] <<24) | (buffer[5] << 16) | (buffer[6] << 8) | buffer[7];
	global_inform->demarcate_inform.concentrate_can_id = (buffer[12] << 24) | (buffer[13] << 16) | (buffer[14] << 8) | buffer[15];

	global_inform->demarcate_inform.board_id = (buffer[68] << 24) | (buffer[69] << 16) | (buffer[70] << 8) | buffer[71];
	
	memcpy(global_inform->demarcate_inform.passwd ,buffer + 90, 8);
	
	global_inform->demarcate_inform.already_save_passwd = buffer[98];
	
	//port num edcode		
	unsigned int offset = 100;
	for(int i = 0; i < 12; i++){
		memcpy(global_inform->demarcate_inform.port_encode[i], buffer + offset, 40);
		offset += 40;
	}
	return 0;
}


unsigned int write_demarcate_256_value(Global_Inform_* global_inform)
{
	char buffer[1024];
	unsigned int i = 0;
//can id 
	buffer[0] = 0;
	buffer[1] = 0;
	buffer[2] = 0;
	buffer[3] = 0;
	buffer[4] = (global_inform->demarcate_inform.can_id >> 24) & 0xff;
	buffer[5] = (global_inform->demarcate_inform.can_id >> 16) & 0xff;
	buffer[6] = (global_inform->demarcate_inform.can_id >>8) & 0xff;
	buffer[7] = global_inform->demarcate_inform.can_id & 0xff;
//concentrator can id 	
	buffer[8] = 0;
	buffer[9] = 0;
	buffer[10] = 0;
	buffer[11] = 0;
	buffer[12] = (global_inform->demarcate_inform.concentrate_can_id >> 24) & 0xff;
	buffer[13] = (global_inform->demarcate_inform.concentrate_can_id >> 16) & 0xff;
	buffer[14] = (global_inform->demarcate_inform.concentrate_can_id >>8) & 0xff;
	buffer[15] = global_inform->demarcate_inform.concentrate_can_id & 0xff;	
	//board id 
	buffer[68] = (global_inform->demarcate_inform.board_id >> 24) & 0xff;
	buffer[69] = (global_inform->demarcate_inform.board_id >> 16) & 0xff;
	buffer[70] = (global_inform->demarcate_inform.board_id >> 8) & 0xff;
	buffer[71] = global_inform->demarcate_inform.board_id & 0xff;
	
	//passwd
	memcpy(buffer + 90 ,global_inform->demarcate_inform.passwd, 8);

	//already save passwd
	buffer[98] = global_inform->demarcate_inform.already_save_passwd;
	
	//port num edcode	
	unsigned int offset = 100;
	for(i = 0; i < 12; i++){
		for(unsigned int j = 0; j < 40; j++)
			buffer[offset + j ] = global_inform->demarcate_inform.port_encode[i][j];
	  offset += 40;
	}
	
	FlashSectorErase(SECTOR_0_START_ADDR);
	FlashPageProgram(SECTOR_0_START_ADDR, (unsigned char *)buffer, 255);
	return 0;
}

unsigned int write_demarcate_value(Global_Inform_* global_inform)
{
	char buffer[1024];
	unsigned int i = 0;
//can id 
	buffer[0] = 0;
	buffer[1] = 0;
	buffer[2] = 0;
	buffer[3] = 0;
	buffer[4] = (global_inform->demarcate_inform.can_id >> 24) & 0xff;
	buffer[5] = (global_inform->demarcate_inform.can_id >> 16) & 0xff;
	buffer[6] = (global_inform->demarcate_inform.can_id >>8) & 0xff;
	buffer[7] = global_inform->demarcate_inform.can_id & 0xff;
//concentrator can id 	
	buffer[8] = 0;
	buffer[9] = 0;
	buffer[10] = 0;
	buffer[11] = 0;
	buffer[12] = (global_inform->demarcate_inform.concentrate_can_id >> 24) & 0xff;
	buffer[13] = (global_inform->demarcate_inform.concentrate_can_id >> 16) & 0xff;
	buffer[14] = (global_inform->demarcate_inform.concentrate_can_id >>8) & 0xff;
	buffer[15] = global_inform->demarcate_inform.concentrate_can_id & 0xff;	
	//board id 
	buffer[68] = (global_inform->demarcate_inform.board_id >> 24) & 0xff;
	buffer[69] = (global_inform->demarcate_inform.board_id >> 16) & 0xff;
	buffer[70] = (global_inform->demarcate_inform.board_id >> 8) & 0xff;
	buffer[71] = global_inform->demarcate_inform.board_id & 0xff;
	
	//passwd
	memcpy(buffer + 90 ,global_inform->demarcate_inform.passwd, 8);

	//already save passwd
	buffer[98] = global_inform->demarcate_inform.already_save_passwd;
	
	//port num edcode	
	unsigned int offset = 100;
	for(i = 0; i < 12; i++){
		for(unsigned int j = 0; j < 40; j++)
			buffer[offset + j ] = global_inform->demarcate_inform.port_encode[i][j];
	  offset += 40;
	}
	
	FlashSectorErase(SECTOR_0_START_ADDR);
	FlashSectorErase(SECTOR_1_START_ADDR);
	FlashSectorErase(SECTOR_2_START_ADDR);
	FlashPageProgram(SECTOR_0_START_ADDR, (unsigned char *)buffer, 255);
	FlashPageProgram(SECTOR_1_START_ADDR, (unsigned char *)(buffer + 256), 255);
	FlashPageProgram(SECTOR_2_START_ADDR, (unsigned char *)(buffer + 512), 255);
	return 0;
}

unsigned int write_somedemarcate_value(Global_Inform_* global_inform)
{
	char buffer[1024];
	unsigned int i = 0;
//can id 
	buffer[0] = 0;
	buffer[1] = 0;
	buffer[2] = 0;
	buffer[3] = 0;
	buffer[4] = (global_inform->demarcate_inform.can_id >> 24) & 0xff;
	buffer[5] = (global_inform->demarcate_inform.can_id >> 16) & 0xff;
	buffer[6] = (global_inform->demarcate_inform.can_id >>8) & 0xff;
	buffer[7] = global_inform->demarcate_inform.can_id & 0xff;
//concentrator can id 	
	buffer[8] = 0;
	buffer[9] = 0;
	buffer[10] = 0;
	buffer[11] = 0;
	buffer[12] = (global_inform->demarcate_inform.concentrate_can_id >> 24) & 0xff;
	buffer[13] = (global_inform->demarcate_inform.concentrate_can_id >> 16) & 0xff;
	buffer[14] = (global_inform->demarcate_inform.concentrate_can_id >>8) & 0xff;
	buffer[15] = global_inform->demarcate_inform.concentrate_can_id & 0xff;	
	//board id 
	buffer[68] = (global_inform->demarcate_inform.board_id >> 24) & 0xff;
	buffer[69] = (global_inform->demarcate_inform.board_id >> 16) & 0xff;
	buffer[70] = (global_inform->demarcate_inform.board_id >> 8) & 0xff;
	buffer[71] = global_inform->demarcate_inform.board_id & 0xff;
	
	//passwd
	//memcpy(buffer + 90 ,global_inform->demarcate_inform.passwd, 8);

	//already save passwd
	buffer[98] = global_inform->demarcate_inform.already_save_passwd;
	
	//port num edcode	
	unsigned int offset = 100;
	for(i = 0; i < 4; i++){
		for(unsigned int j = 0; j < 40; j++)
			buffer[offset + j ] = global_inform->demarcate_inform.port_encode[i][j];
	  offset += 40;
	}
	
	FlashSectorErase(SECTOR_0_START_ADDR);
	FlashPageProgram(SECTOR_0_START_ADDR, (unsigned char *)buffer, 255);
	return 0;
}
