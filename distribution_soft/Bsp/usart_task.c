#include "usart_task.h"
#include "common.h"
#include "cmsis_os.h"
#include "stdio.h"
#include "string.h"
#include "stdlib.h"
#include "w25q16.h"
#include "usart.h"

#define USART_HOLDKEYTIME 50

static unsigned char usart_status=0;
extern unsigned char receive_flag;
extern char receive_buf[100];

void usart_task(void * argument)
{
	Global_Inform_* global_inform = (Global_Inform_ *)argument;
  while(1)
  {
		if (xQueueReceive(global_inform->dispatch_inform.usart_queue, &usart_status, USART_HOLDKEYTIME/portTICK_RATE_MS) == pdPASS)
		{
			if(receive_buf[1] == 'W')
			{
				if(receive_buf[2] == '0' && receive_buf[3] == '1'){    //can id
					unsigned int value = 0;
					for(unsigned int j = 0; j < 8; j++){
						unsigned char tmp;
						if(receive_buf[4+j]>='a' && receive_buf[4+j]<='f')
							tmp = receive_buf[4+j] - 'a' + 10;
						else if(receive_buf[4+j]>='A' && receive_buf[4+j]<='F')
							tmp = receive_buf[4+j] - 'A' + 10;
						else 
							tmp = receive_buf[4+j] - '0';
						value |= tmp << ((7 - j) * 4);
					}
					global_inform->demarcate_inform.can_id = value;
					printf("FW01%08x\n\r",global_inform->demarcate_inform.can_id);
				}
				else if(receive_buf[2] == '0' && receive_buf[3] == '2')     //board id  
				{ 
					unsigned int value = 0;
					for(unsigned int j = 0; j < 8; j++){
						unsigned char tmp;
						if(receive_buf[4+j]>='a' && receive_buf[4+j]<='f')
							tmp = receive_buf[4+j] - 'a' + 10;
						else if(receive_buf[4+j]>='A' && receive_buf[4+j]<='F')
							tmp = receive_buf[4+j] - 'A' + 10;
						else 
							tmp = receive_buf[4+j] - '0';
						value |= tmp << ((7 - j) * 4);
					}
					global_inform->demarcate_inform.board_id = value;
					printf("FW02%08x\n\r",global_inform->demarcate_inform.board_id);
				}
				else if((receive_buf[2] == 'e' || receive_buf[2] == 'E') && (receive_buf[3] == '1' || receive_buf[3] == '2' || receive_buf[3] == '3' || receive_buf[3] == '4' || \
																					receive_buf[3] == '5' || receive_buf[3] == '6' || receive_buf[3] == '7' || receive_buf[3] == '8' || \
																					receive_buf[3] == '9' || receive_buf[3] == 'a' || receive_buf[3] == 'b' || receive_buf[3] == 'c' || \
																					receive_buf[3] == 'A' || receive_buf[3] == 'B' || receive_buf[3] == 'C'))
				{
					unsigned char index =0;
					if(receive_buf[3] == 'a' || receive_buf[3] == 'b' || receive_buf[3] == 'c')
						index = receive_buf[3] - 'a' + 9;
					else if(receive_buf[3] == 'A' || receive_buf[3] == 'B' || receive_buf[3] == 'C')
						index = receive_buf[3] - 'A' + 9;
					else
						index = receive_buf[3] - '1';	
					unsigned num_ = query_info_num();
					for(unsigned int i = 0; i < num_; i++)
						global_inform->demarcate_inform.port_encode[index][i] = receive_buf[ 4+ i];
					for(unsigned int i = num_ - 4 ; i < 40; i++)
					  global_inform->demarcate_inform.port_encode[index][i] = ' ';
					printf("FWE%c%.*s\n\n\r", receive_buf[3],40, global_inform->demarcate_inform.port_encode[index]);
				}			
				else if((receive_buf[2] == 'S' || receive_buf[2] == 's') && (receive_buf[3] == 'V' || receive_buf[3] == 'v')){
					printf("FWSV00000000\n\r");
					//vTaskSuspendAll();
					write_demarcate_value(global_inform);
					//HAL_Delay(10);
					//xTaskResumeAll();
					printf("Save Finish\n\r");
				}			
				else if((receive_buf[2] == 'R' || receive_buf[2] == 'r') && (receive_buf[3] == 'e' || receive_buf[3] == 'E')){
					printf("--------------------------system reset---------------------------\n\r");
					__disable_irq();
					HAL_NVIC_SystemReset();
				}					
			}
		}
	}
}


