#ifndef __LDC_1614_H
#define __LDC_1614_H

typedef int s32;
typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;

#define CHANNEL_0  0
#define CHANNEL_1  1
#define CHANNEL_2  2
#define CHANNEL_3  3
#define CHANNEL_NUM  4

#define DEFAULT_IIC_ADDR  0x2A
#define IIC_ADDR_R 0X55
#define IIC_ADDR_W 0X54

//#define DEFAULT_IIC_ADDR  0x2B
//#define IIC_ADDR_R 0X57
//#define IIC_ADDR_W 0X56

/*Register Rddr*/
/***************************************************************************/
#define CONVERTION_RESULT_REG_START             0X00
#define SET_CONVERSION_TIME_REG_START           0X08
#define SET_CONVERSION_OFFSET_REG_START         0X0C
#define SET_LC_STABILIZE_REG_START              0X10
#define SET_FREQ_REG_START                      0X14

#define SENSOR_STATUS_REG                       0X18
#define ERROR_CONFIG_REG                        0X19
#define SENSOR_CONFIG_REG                       0X1A
#define MUL_CONFIG_REG                          0X1B
#define SENSOR_RESET_REG                        0X1C
#define SET_DRIVER_CURRENT_REG                  0X1E

#define READ_MANUFACTURER_ID                    0X7E
#define READ_DEVICE_ID                          0X7F

/******************************************************************************/
/*ERROR bit*/
#define UR_ERR2OUT                              ((u16)1<<15)
#define OR_ERR2OUT                              ((u16)1<<14)
#define WD_ERR2OUT                              ((u16)1<<13)
#define AH_ERR2OUT                              ((u16)1<<12)
#define AL_ERR2OUT                              ((u16)1<<11)
#define UR_ERR2INT                              ((u16)1<<7)
#define OR_ERR2INT                              ((u16)1<<6)
#define WD_ERR2INT                              ((u16)1<<5)
#define AH_ERR2INT                              ((u16)1<<4)
#define AL_ERR2INT                              ((u16)1<<3)
#define ZC_ERR2INT                              ((u16)1<<2)
#define DRDY_2INT                               ((u16)1<<0)


/******************************************************************************/
/*SENSOR CONFIG bit*/
#define ACTIVE_CHANNEL                         ( ((u16)1<<15) | ((u16)1<<14) )
#define SLEEP_MODE_EN                           ((u16)1<<13)
#define RP_OVERRIDE_EN                          ((u16)1<<12)
#define SENSOR_ACTIVATE_SEL                     ((u16)1<<11)
#define AUTO_AMP_DIS                            ((u16)1<<10)
#define REF_CLK_SRC                             ((u16)1<<9)
#define INTB_DIS                                ((u16)1<<7)
#define HIGH_CURRENT_DRV                        ((u16)1<<6)
/*!!!!
*The low bit 0~5 are reserved,must set to 000001.
*/

unsigned short ldc1614_read_reg(unsigned char device_id, unsigned char reg_address);

void ldc1614_write_reg(unsigned char device_id, unsigned char reg_address,u16 data);

int LDC1614_read_sensor_infomation(u8 id);
u16 LDC1614_get_sensor_status(u8 id);
u32 LDC1614_get_channel_result(u8 id, u8 channel);

s32 LDC1614_single_channel_config(u8 id, u8 channel);
void ldc1614_iic_init(void);
void ldc1614_init(void);
void ldc1614_speic_reset(unsigned char device_id);
void ldc1614_single_init_device(unsigned char device_id);
#endif





