#ifndef __WWDG_H
#define __WWDG_H

#include "stm32f4xx_hal.h"
#include "cmsis_os.h"

void wwdg_init(void);
void iwdg_init(void);
void iwdg_refresh(void);
#endif


