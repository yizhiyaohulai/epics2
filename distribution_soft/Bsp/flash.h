#ifndef __FLASH_H
#define __FLASH_H

#include "common.h"

void flash_write(Global_Inform_* global_inform);
unsigned int flash_read(Global_Inform_* global_inform);

#endif


