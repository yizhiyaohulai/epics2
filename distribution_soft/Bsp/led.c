#include "led.h"
#include "common.h"
#include "stm32f4xx_hal.h"

unsigned int Led_Init(void){

	GPIO_InitTypeDef GPIO_InitStruct;
	
	 __HAL_RCC_GPIOB_CLK_ENABLE();
	 __HAL_RCC_GPIOC_CLK_ENABLE();
	 __HAL_RCC_GPIOD_CLK_ENABLE(); 
	 __HAL_RCC_GPIOE_CLK_ENABLE();

	struct CONTROL_* led_control_ = GetControlMethod();
	
	for(unsigned i = 0; i < PORT_NUM; i++){
	  GPIO_InitStruct.Pin = led_control_[i].Pin;
		GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
		HAL_GPIO_Init(led_control_[i].GPIOx, &GPIO_InitStruct);
		HAL_GPIO_WritePin(led_control_[i].GPIOx, led_control_[i].Pin,GPIO_PIN_RESET);
	}
	return 1;
}






