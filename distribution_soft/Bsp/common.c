#include "common.h"

struct CONTROL_ key_input[PORT_NUM] = {{GPIOD, GPIO_PIN_2}, {GPIOD, GPIO_PIN_3},  {GPIOD, GPIO_PIN_4}, \
																		  {GPIOD, GPIO_PIN_5},  {GPIOD, GPIO_PIN_6},  {GPIOD, GPIO_PIN_7}, \
																		  {GPIOD, GPIO_PIN_8},  {GPIOD, GPIO_PIN_9},  {GPIOD, GPIO_PIN_10}, \
																		  {GPIOD, GPIO_PIN_11}, {GPIOD, GPIO_PIN_12}, {GPIOD, GPIO_PIN_13}};	



struct CONTROL_ led_control_[12] = {{GPIOE, GPIO_PIN_2},  {GPIOE, GPIO_PIN_3},  {GPIOE, GPIO_PIN_4}, \
																		{GPIOE, GPIO_PIN_5},  {GPIOE, GPIO_PIN_6},  {GPIOE, GPIO_PIN_7}, \
																		{GPIOE, GPIO_PIN_8},  {GPIOE, GPIO_PIN_9},  {GPIOE, GPIO_PIN_10}, \
																		{GPIOE, GPIO_PIN_11}, {GPIOE, GPIO_PIN_12}, {GPIOE, GPIO_PIN_13}};			


struct CONTROL_* GetInputEvent(void){
	return key_input;
}

struct CONTROL_* GetControlMethod(void){
	return led_control_;
}


////////////////////////////////////////////////////////////
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;

    /**Configure the main internal regulator output voltage 
    */
  __HAL_RCC_PWR_CLK_ENABLE();

  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
	RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    //Error_Handler();
  }

	/**Initializes the CPU, AHB and APB busses clocks 
	*/
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSE;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    //Error_Handler();
  }

  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);
  HAL_NVIC_SetPriority(SysTick_IRQn, 15, 0);
}

