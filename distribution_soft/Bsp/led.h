#ifndef _LED_H
#define _LED_H

unsigned int Led_Init(void);


#define  LED1(a)  if(a) 	HAL_GPIO_WritePin(GPIOE, GPIO_PIN_2, GPIO_PIN_SET); \
									else 	HAL_GPIO_WritePin(GPIOE, GPIO_PIN_2, GPIO_PIN_RESET)

#define  LED2(a)  if(a) 	HAL_GPIO_WritePin(GPIOE, GPIO_PIN_3, GPIO_PIN_SET); \
									else 	HAL_GPIO_WritePin(GPIOE, GPIO_PIN_3, GPIO_PIN_RESET)
									
#define  LED3(a)  if(a) 	HAL_GPIO_WritePin(GPIOE, GPIO_PIN_4, GPIO_PIN_SET); \
									else 	HAL_GPIO_WritePin(GPIOE, GPIO_PIN_4, GPIO_PIN_RESET)		
									
#define  LED4(a)  if(a) 	HAL_GPIO_WritePin(GPIOE, GPIO_PIN_5, GPIO_PIN_SET); \
									else 	HAL_GPIO_WritePin(GPIOE, GPIO_PIN_5, GPIO_PIN_RESET)									
									
#define  LED5(a)  if(a) 	HAL_GPIO_WritePin(GPIOE, GPIO_PIN_6, GPIO_PIN_SET); \
									else 	HAL_GPIO_WritePin(GPIOE, GPIO_PIN_6, GPIO_PIN_RESET)
									
#define  LED6(a)  if(a) 	HAL_GPIO_WritePin(GPIOE, GPIO_PIN_7, GPIO_PIN_SET); \
									else 	HAL_GPIO_WritePin(GPIOE, GPIO_PIN_7, GPIO_PIN_RESET)									

#define  LED7(a)  if(a) 	HAL_GPIO_WritePin(GPIOE, GPIO_PIN_8, GPIO_PIN_SET); \
									else 	HAL_GPIO_WritePin(GPIOE, GPIO_PIN_8, GPIO_PIN_RESET)

#define  LED8(a)  if(a) 	HAL_GPIO_WritePin(GPIOE, GPIO_PIN_9, GPIO_PIN_SET); \
									else 	HAL_GPIO_WritePin(GPIOE, GPIO_PIN_9, GPIO_PIN_RESET)
									
#define  LED9(a)  if(a) 	HAL_GPIO_WritePin(GPIOE, GPIO_PIN_10, GPIO_PIN_SET); \
									else 	HAL_GPIO_WritePin(GPIOE, GPIO_PIN_10, GPIO_PIN_RESET)		
									
#define  LED10(a)  if(a) 	HAL_GPIO_WritePin(GPIOE, GPIO_PIN_11, GPIO_PIN_SET); \
									else 	HAL_GPIO_WritePin(GPIOE, GPIO_PIN_11, GPIO_PIN_RESET)									
									
#define  LED11(a)  if(a) 	HAL_GPIO_WritePin(GPIOE, GPIO_PIN_12, GPIO_PIN_SET); \
									else 	HAL_GPIO_WritePin(GPIOE, GPIO_PIN_12, GPIO_PIN_RESET)
									
#define  LED12(a)  if(a) 	HAL_GPIO_WritePin(GPIOE, GPIO_PIN_13, GPIO_PIN_SET); \
									else 	HAL_GPIO_WritePin(GPIOE, GPIO_PIN_13, GPIO_PIN_RESET)										
									
#endif 



