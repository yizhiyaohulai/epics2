#include "m_math.h"
#include "string.h"
#include "stdio.h"
#include "math.h"
#include "stdlib.h"

int cmpfunc (const void * a, const void * b)
{
   return ( *(int*)a - *(int*)b );
}

void QueueInit(struct FifoQueue *Queue)
{
	Queue->front = Queue->rear;
	Queue->count = 0;
}

unsigned int QueueIn(struct FifoQueue *Queue, unsigned int sdat)
{
	unsigned int status =0;
	if ((Queue->front == Queue->rear) && (Queue->count == QueueSize))
	{
		return QueueFull;
	}
	else
	{
		unsigned int tmp[QueueSize]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
		
		Queue->dat[Queue->rear] = sdat;
		Queue->rear = (Queue->rear + 1) % QueueSize;
		Queue->count = Queue->count + 1;

		memcpy(tmp, Queue->dat,  sizeof(unsigned int)*Queue->count);
		qsort(tmp, Queue->count, sizeof(unsigned int), cmpfunc);
		
		unsigned int sum = 0;
		
		if(Queue->count < QueueSize){
			for(unsigned int i = 0; i < Queue->count; i++)
				sum += tmp[i];
			Queue->ave = sum / Queue->count;
		}
		else{
			for (unsigned int i = 0; i < ceil(Queue->count *1.0f /2); i++)
				sum += tmp[i + (Queue->count  / 4) ];
			Queue->ave = sum / ceil(Queue->count/2);
		}
		
		Queue->min = tmp[0] < Queue->min ? tmp[0] : Queue->min;
		Queue->max = tmp[Queue->count - 1] > Queue->max ? tmp[Queue->count - 1] : Queue->max;
		
		return (status == 0)?QueueOperateOk:QueueStatusChange;
	}
}

// Queue Out
unsigned int QueueOut(struct FifoQueue *Queue)
{
	if ((Queue->front == Queue->rear) && (Queue->count == 0))
	{                    // empty
		return QueueEmpty;
	}
	else
	{                    // out
		Queue->front = (Queue->front + 1) % QueueSize;
		Queue->count = Queue->count - 1;
		return QueueOperateOk;
	}
}

void  QueueClear(struct FifoQueue *Queue){
	
	Queue->max = 0;
	Queue->min = 0xffffffff;
}




