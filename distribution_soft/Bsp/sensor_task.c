#include "common.h"
#include "cmsis_os.h"
#include "m_math.h"
#include "key.h" 
#include "sensor_task.h"

struct FifoQueue port0_queue[12];

#define SWITCH_QUEUE_FIFO 1
#define SWITCH_HOLDKEYTIME 750

enum Operator{
	NO_OPERATOR,
	SHORT_OPERATOR,
	LONG_OPERATOR,
	RELEASE_OPERATOR
};

static unsigned char switch_status[PORT_NUM] = { 0,0,0,0, \
																				  0,0,0,0, \
																				  0,0,0,0};
static unsigned int xTicksStart[PORT_NUM],xTicksEnd[PORT_NUM];
static unsigned char switch_hold_status[PORT_NUM] = { 0,0,0,0, \
																									    0,0,0,0, \
																											0,0,0,0};
static Global_Inform_* switch_inform;
static QueueHandle_t switch_queue;

void sensor_task(void* argument)
{
	//for(signed i = 0 ; i < 12; i++)
	//	port0_queue[i].min = port0_queue[i].max;
	switch_inform = (Global_Inform_ *)argument;
	switch_queue = xQueueCreate(SWITCH_QUEUE_FIFO, sizeof(unsigned char));
	key_init(switch_inform);
	unsigned char status = 0;
  while(1)
  {
		if(xQueueReceive(switch_queue, &status, 500/portTICK_RATE_MS) == pdPASS){
			unsigned char index = status >> 4;
			unsigned char value = status & 0x0f;
			//--------------------------------------------Short AND Release OPTION--------------------------------------------------//
			if(value == SHORT_OPERATOR){
				xEventGroupSetBits(switch_inform->dispatch_inform.xEventGroup, 1 << (index * 2));
				switch_status[index] = RELEASE_OPERATOR;
				printf("short press :%d\n\r",index);
			}
			else if(value == RELEASE_OPERATOR){
				printf("Release :%d\n\r",index);
				switch_status[index] = NO_OPERATOR;
				xEventGroupSetBits(switch_inform->dispatch_inform.xEventGroup, 1 << (index * 2 + 1));
			}
		}
		else{
			for(unsigned char i = 0; i < PORT_NUM; i++)
			{
				if(switch_status[i] == RELEASE_OPERATOR){
						printf("No Event Release :%d\n\r",i);
						switch_status[i] = NO_OPERATOR;
						xEventGroupSetBits(switch_inform->dispatch_inform.xEventGroup, 1 << (i * 2 + 1));
				}
				//--------------------------------------------lONG OPTION--------------------------------------------------//
				int value = xTaskGetTickCount() - xTicksStart[i];
				if((switch_hold_status[i] == 1)&&((xTaskGetTickCount() - xTicksStart[i]) >= (SWITCH_HOLDKEYTIME/portTICK_RATE_MS)))  //Said  hold key more than five sec 
				{
					xEventGroupSetBits(switch_inform->dispatch_inform.xEventGroup, 1 << (i * 2));
					switch_hold_status[i] = 0;
					printf("long operator %d\n\r", i);
				}
			}		
		}
	}
}


void CallBackFunction(unsigned i){
	struct CONTROL_* led_control_ = GetControlMethod();
	struct CONTROL_* key_input = GetInputEvent();
	if(HAL_GPIO_ReadPin(key_input[i].GPIOx, key_input[i].Pin) == 0)
	{
		xTicksStart[i]=xTaskGetTickCountFromISR();
		switch_hold_status[i]= 1;
		if((switch_inform->dispatch_inform.test_status[i] == 0) && \
			 (switch_inform->dispatch_inform.init_test_status[i] == 0) && \
			 (switch_inform->dispatch_inform.malfunction_status[i] == 0)){
					HAL_GPIO_WritePin(led_control_[i].GPIOx, led_control_[i].Pin,GPIO_PIN_SET);
					switch_inform->dispatch_inform.led_status[i] = 1;
		}
	}
	else if(HAL_GPIO_ReadPin(key_input[i].GPIOx, key_input[i].Pin) == 1)																														  //realse
	{
		xTicksEnd[i]=xTaskGetTickCountFromISR();
		BaseType_t xHigherPriorityTaskWoken = pdFALSE;
		unsigned int cmd = 0;
		if((xTicksEnd[i] - xTicksStart[i]) <20){
			cmd = RELEASE_OPERATOR | (i << 4);
			xTicksStart[i] = xTicksEnd[i];
			switch_hold_status[i] = 0;
			xQueueSendFromISR(switch_queue, &cmd,&xHigherPriorityTaskWoken);			
		}
		else if((xTicksEnd[i] - xTicksStart[i])> 20 && (xTicksEnd[i] - xTicksStart[i]) < SWITCH_HOLDKEYTIME)
		{
			cmd = SHORT_OPERATOR | (i << 4);
			xTicksStart[i]= xTicksEnd[i];
			switch_hold_status[i] = 0;
			xQueueSendFromISR(switch_queue, &cmd,&xHigherPriorityTaskWoken);
		}
		else if((xTicksEnd[i] - xTicksStart[i]) > SWITCH_HOLDKEYTIME){
			cmd = RELEASE_OPERATOR | (i << 4);
			xTicksStart[i]=xTicksEnd[i];
			switch_hold_status[i] = 0;
			xQueueSendFromISR(switch_queue, &cmd,&xHigherPriorityTaskWoken);			
		}
		if((switch_inform->dispatch_inform.test_status[i] == 0) && \
			(switch_inform->dispatch_inform.init_test_status[i] == 0) && \
			(switch_inform->dispatch_inform.malfunction_status[i] == 0)){
				HAL_GPIO_WritePin(led_control_[i].GPIOx, led_control_[i].Pin,GPIO_PIN_RESET);
				switch_inform->dispatch_inform.led_status[i] = 0;
		}
	}
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin){
	for(unsigned int i = 0; i < PORT_NUM; i++){
		if(GetInputEvent()[i].Pin == GPIO_Pin){
			CallBackFunction(i);
			break;
		}
	}
}
