#include "usart.h"
#include "stm32f4xx_hal.h"
#include "cmsis_os.h"


UART_HandleTypeDef huart2;
unsigned char relay_buf[1];
char receive_buf[100];
unsigned char receive_num = 0;
unsigned char info_num = 0;
Global_Inform_* usart_inform;

unsigned int Usart_Init(Global_Inform_* global_inform){

	GPIO_InitTypeDef GPIO_InitStruct;
	
	__HAL_RCC_USART2_CLK_ENABLE();
	__HAL_RCC_GPIOA_CLK_ENABLE();
	
	usart_inform = global_inform;
	global_inform->dispatch_inform.usart_queue  = xQueueCreate(1, sizeof(unsigned char));
	
	/**USART1 GPIO Configuration    
	PA2     ------> USART1_TX
	PA3     ------> USART1_RX 
	*/
	GPIO_InitStruct.Pin = GPIO_PIN_2;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = GPIO_PIN_3;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
	unsigned char status = HAL_UART_Init(&huart2);
	
	/* Peripheral interrupt init */
	HAL_NVIC_SetPriority(USART2_IRQn, 14, 0);
	HAL_NVIC_EnableIRQ(USART2_IRQn);	
	//unsigned char value = 0XFF;
	//HAL_UART_Transmit(&huart2, &value, 1, 0xffff);
	HAL_UART_Receive_IT(&huart2, relay_buf, 1);
	return status;
}

int fputc(int ch,FILE *f)
{
	HAL_UART_Transmit(&huart2, (uint8_t *)&ch, 1, 0xFFFF);
	return ch;
}

unsigned char query_info_num(){
	return info_num;
}
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *UartHandle){
	if(UartHandle->Instance == USART2){
		unsigned char sbuf = relay_buf[0];
		if((receive_num==0)&&(sbuf=='F'))
		{
			receive_buf[0]=sbuf;
			receive_num++;
		}
		else
		{
			if((receive_num==0)&&(sbuf!='F'))
			{
				receive_num=0;
			}
			else
			{
				if((sbuf=='W' || sbuf=='R')&&(receive_num==1))
				{
					receive_buf[1]=sbuf;
					receive_num++;
				}
				else
				{
					if((sbuf!='W'|| sbuf=='R')&&(receive_num==1))
					{
						receive_num=0;
					}
					else
					{
						receive_buf[receive_num]=sbuf;
						receive_num++; 
						
						if((receive_buf[3]=='1' || receive_buf[3]=='2' || receive_buf[3]=='3' || receive_buf[3]=='4' || \
							 receive_buf[3]=='5' || receive_buf[3]=='6' || receive_buf[3]=='7' || receive_buf[3]=='8' || \
							 receive_buf[3]=='9' || receive_buf[3]=='a' || receive_buf[3]=='b' || receive_buf[3]=='c') && (receive_buf[2]=='E' || receive_buf[2]=='e'))
						{
							if(receive_num >= 44 || sbuf == '\r')
							{
								info_num = receive_num - 1;
								receive_num=0; 		
								BaseType_t xHigherPriorityTaskWoken = pdFALSE;
							  unsigned int cmd = USART_FINISH_TRANSFER;
								xQueueSendFromISR(usart_inform->dispatch_inform.usart_queue, &cmd,&xHigherPriorityTaskWoken);						
							}
						}
						else
						{
							if(receive_num>=12)
							{
								receive_num=0;		  
								BaseType_t xHigherPriorityTaskWoken = pdFALSE;
								unsigned int cmd = USART_FINISH_TRANSFER;
								xQueueSendFromISR(usart_inform->dispatch_inform.usart_queue, &cmd,&xHigherPriorityTaskWoken);
							}
						}
					}
				}
			}
		}
		HAL_UART_Receive_IT(&huart2, relay_buf, 1);
	}
}


