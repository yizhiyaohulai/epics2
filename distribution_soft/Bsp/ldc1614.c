#include "ldc1614.h"
#include "stm32f4xx_hal.h"
#include "math.h"

I2C_HandleTypeDef i2c_handle[3];

//1
unsigned int sample_[12]= {(15<<11),(15<<11),(15<<11),(15<<11),\
													(15<<11),(15<<11),(15<<11),(15<<11),\
													(15<<11),(15<11),(15<<11),(15<<11)};


static float resistance[CHANNEL_NUM];
static float inductance[CHANNEL_NUM];
static float capacitance[CHANNEL_NUM];
static float Fref[CHANNEL_NUM];
static float Fsensor[CHANNEL_NUM];
static float Q_factor[CHANNEL_NUM];
static u16 settling_time[CHANNEL_NUM]; //ceil(Q_factor[channel] * Fref[channel] / (16 * Fsensor[channel]));
static u16 conver_time[CHANNEL_NUM];

const char *status_str[]={
	"conversion under range error",
	"conversion over range error",
  "watch dog timeout error",
	"Amplitude High Error",
  "Amplitude Low Error",
	"Zero Count Error",
  "Data Ready",
	"unread conversion is present for channel 0",
  "unread conversion is present for Channel 1.",
  "unread conversion ispresent for Channel 2.",
  "unread conversion is present for Channel 3."
};

unsigned short ldc1614_read_reg(unsigned char device_id, unsigned char reg_address)
{				  
	unsigned char txbuf[1] = {reg_address}; 
	unsigned char rxbuf[2] = {0, 0};
	HAL_I2C_Master_Transmit(&i2c_handle[device_id], IIC_ADDR_R, txbuf, 1, 0xfff);
	HAL_I2C_Master_Receive(&i2c_handle[device_id], IIC_ADDR_W, rxbuf, 2, 0xfff);
	unsigned short temp = (rxbuf[0] << 8 ) | rxbuf[1];
	
	return temp;
}

void ldc1614_write_reg(unsigned char device_id, unsigned char reg_address, u16 value)
{		
	unsigned char txbuf[3] = {reg_address, value>>8,value&0xff}; 
	HAL_I2C_Master_Transmit(&i2c_handle[device_id], IIC_ADDR_W, (unsigned char*)&txbuf, 3, 0xfff);
}


void LDC1614_set_Rp(u8 channel,float n_kom)
{
    resistance[channel]=n_kom;
}
void LDC1614_set_L(u8 channel,float n_uh)
{
    inductance[channel]=n_uh;
}
void LDC1614_set_C(u8 channel,float n_pf)
{
    capacitance[channel]=n_pf;
}

void LDC1614_set_Q_factor(u8 channel,float q)
{
    Q_factor[channel]=q;
}

void calc_settling_time(u8 channel)
{
		//setting time 20us 
		//settling_time[channel] = ceil( Q_factor[channel] * Fref[channel] / (16 * Fsensor[channel]) ) +1;
	  settling_time[channel] = 0x10;
	  //printf("settlingtime[%d]= 0x%x\n",channel, settling_time[channel]);
}

void calc_conver_time(u8 channel)
{
		//conv time 8us 
		conver_time[channel] = 0x04D6;     
	  //printf("conver_time[%d]= 0x%x\n",channel, conver_time[channel]);
}

void LDC1614_sensor_status_parse(u16 value)
{
    u16 section = 0;
    section = value>>14;
    switch(section)
    {
        case 0:printf("Channel 0 is source of flag or error.");
        break;
        case 1:printf("Channel 1 is source of flag or error.");
        break;
        case 2:printf("Channel 2 is source of flag or error.");
        break;
        case 3:printf("Channel 3 is source of flag or error.");
        break;
        default:
        break;
    }
    for(u32 i=0;i<6;i++)
    {
        if(value & (u16)1<<(8+i) )
        {
            printf("%s",status_str[6-i]);
        }
    }
    if(value&(1<<6))
    {
        printf("%s",status_str[6]);
    }
    for(u32 i=0;i<4;i++)
    {
        if(value & (1<<i) )
        {
            printf("%s",status_str[10-i]);
        }
    }
}

u16 LDC1614_get_sensor_status(u8 id)
{
    u16 value=0;
	  value = ldc1614_read_reg(id, SENSOR_STATUS_REG);
		//printf("status = 0x%x \n", value);
    LDC1614_sensor_status_parse(value);
    return value;
}

int LDC1614_read_sensor_infomation(u8 id)
{
		printf("device id =%d \n\r",id);
	
    u16 value=0;
    value = ldc1614_read_reg(id, READ_MANUFACTURER_ID);
    printf("manufacturer id =0x%x \n\r",value);
  
    value = ldc1614_read_reg(id, READ_DEVICE_ID);
		printf("DEVICE id  =0x%x \n",value);
	  if(value != 0x3055)
			return -1;

    return 0;
}


s32 LDC1614_parse_result_data(u8 channel,u32 raw_result)
{
    u8 value=0;
	  u32 result =0;
    result = raw_result & 0x0fffffff;
    if(0xfffffff == result)
    {
        printf("can't detect coil Coil Inductance!!!\n\r");
        result=0;
        return -1;
    }
   
    value = raw_result >> 24;
    if(value&0x80)
    {
        printf("channel = %d \n", channel);
        printf(": ERR_UR-Under range error!!!\n\r");
    }
    if(value&0x40)
    {
        printf("channel = %d \n", channel);

        printf(": ERR_OR-Over range error!!!\n\r");
    }
    if(value&0x20)
    {
        printf("channel = %d \n", channel);
        printf(": ERR_WD-Watch dog timeout error!!!\n\r");
    }
    if(value&0x10)
    {
        printf("channel = %d \n", channel);
        printf(": ERR_AE-error!!!\n\r");
    }
    return 0;
}



u32 LDC1614_get_channel_result(u8 id, u8 channel)
{
    u32 raw_value=0;
    
    u16 value=0;
    value = ldc1614_read_reg(id, CONVERTION_RESULT_REG_START+channel*2);
    raw_value|=(u32)value << 16;
		
    value = ldc1614_read_reg(id, CONVERTION_RESULT_REG_START+channel*2+1);
    raw_value|=(u32)value;
		
    LDC1614_parse_result_data(channel, raw_value);
		
    return raw_value;
}

////////////////config 
s32 LDC1614_reset(u8 id)
{
  ldc1614_write_reg(id, SENSOR_RESET_REG, 0x8000);
	return 0;
}

s32 LDC1614_set_FIN_FREF_DIV(u8 id, u8 channel)
{
    u16 value;
    u16 FIN_DIV,FREF_DIV;

    Fsensor[channel] = 1/(2*3.14*sqrt(inductance[channel]*capacitance[channel]*pow(10,-18)))*pow(10,-6);
    //printf("fsensor = %f MHz\n\r", Fsensor[channel]);

    FIN_DIV = (unsigned short)(Fsensor[channel]/8.75+1);

    if(Fsensor[channel] * 4 < 40)
    {
        FREF_DIV = 2;
        Fref[channel] = 40/2;
    }
    else
    {
        FREF_DIV = 2;
        Fref[channel] = 40/2;
    }

    value = FIN_DIV << 12;
    value |= FREF_DIV;
		ldc1614_write_reg(id, SET_FREQ_REG_START+channel, value);
    return 0;
}


s32 LDC1614_set_LC_stabilize_time(u8 id, u8 channel)
{
		//settling_time 20us
		//u16 SETTLECOUNT = Q_factor[channel] * ceil(Fref[channel] / (16 * Fsensor[channel]));
	  //SETTLECOUNT += 10;
		//1us  channel switching delay
		ldc1614_write_reg(id, SET_LC_STABILIZE_REG_START+channel,0x20);//settling_time[channel]);
		return 0;
}

s32 LDC1614_set_conversion_time(u8 id, u8 channel)
{
		ldc1614_write_reg(id, SET_CONVERSION_TIME_REG_START+channel, conver_time[channel]);
    return 0;
}

s32 LDC1614_set_driver_current(u8 id, u8 channel, u16 value)
{
		ldc1614_write_reg(id, SET_DRIVER_CURRENT_REG+channel,value);
    return 0;
}

s32 LDC1614_set_mux_config(u8 id, u16 value)
{
		ldc1614_write_reg(id, MUL_CONFIG_REG, value);
    return 0;
}

s32 LDC1614_set_config(u8 id, u16 value)
{
		ldc1614_write_reg(id, SENSOR_CONFIG_REG, value);
    return 0;
}

void LDC1614_select_channel_to_convert(u8 id, u8 channel, u16 *value)
{
    switch(channel)
    {
        case 0:*value &=0x3fff;
        break;
        case 1:*value &=0x7fff;
               *value |=0x4000;
        break;
        case 2:*value &=0xbfff;
               *value |=0x8000;
        break;
        case 3:*value |=0xc000;
        break;
    }   
}

s32 LDC1614_single_channel_config(u8 id, u8 channel)
{
	// pcb param: layer:4;   turn:11;   diameter:8mm;
	// capacitance: 330pf;   trace width:0.1mm;    space between traces: 0.1mm;
	
    /*Set coil inductor parameter first.*/
    LDC1614_set_Rp(channel,8.402);
    LDC1614_set_L(channel,10.02);
    LDC1614_set_C(channel,330);
    LDC1614_set_Q_factor(channel,48.03);
	
	  // config SLEEP mode
	  LDC1614_set_config(id, 0x2000);
	
		int time = 1000;
	  while(time--);
	
		LDC1614_reset(id);
	
    LDC1614_set_FIN_FREF_DIV(id, channel);
	
		calc_settling_time(channel);
	  calc_conver_time(channel);
    LDC1614_set_LC_stabilize_time(id, channel);

    /*Set conversion interval time*/
    LDC1614_set_conversion_time(id, channel);

    /*Set driver current!*/
    LDC1614_set_driver_current(id, channel, 0xe000);

    /*single conversion*/
    LDC1614_set_mux_config(id, 0x20c);
		
    /*start channel 0*/
    u16 config = 0x1601; // 1601
    LDC1614_select_channel_to_convert(id, channel, &config);
    LDC1614_set_config(id, config);
		
		
//		u16 data = ldc1614_read_reg(id, SENSOR_RESET_REG);
//		printf("reset: reg = 0x%x; data= 0x%x ||||  \n", SENSOR_RESET_REG, data);
//		
//	  data = ldc1614_read_reg(id, SET_FREQ_REG_START);
//		printf("FREQ: reg = 0x%x; data= 0x%x ||||  \n", SET_FREQ_REG_START, data);
//		
//		data = ldc1614_read_reg(id, SET_LC_STABILIZE_REG_START);
//		printf("setling: reg = 0x%x; data= 0x%x ||||  \n", SET_LC_STABILIZE_REG_START, data);
//		
//		data = ldc1614_read_reg(id, SET_CONVERSION_TIME_REG_START);
//		printf("conver: reg = 0x%x; data= 0x%x ||||  \n", SET_CONVERSION_TIME_REG_START, data);
//		
//		data = ldc1614_read_reg(id, SET_DRIVER_CURRENT_REG);
//		printf("current: reg = 0x%x; data= 0x%x ||||  \n", SET_DRIVER_CURRENT_REG,data);
//		
//		data = ldc1614_read_reg(id, MUL_CONFIG_REG);
//		printf("mul: reg = 0x%x; data= 0x%x ||||  \n", MUL_CONFIG_REG,data);
//		
//		data = ldc1614_read_reg(id, SENSOR_CONFIG_REG);
//		printf("config: reg = 0x%x; data= 0x%x ||||  \n", SENSOR_CONFIG_REG, data);
		
    return 0;
}


s32 LDC1614_mul_channel_config(u8 id)
{
	// pcb param: layer:4;   turn:11;   diameter:8mm;
	// capacitance: 330pf;   trace width:0.1mm;    space between traces: 0.1mm;
	
    /*Set coil inductor parameter first.*/
	  for(int channel =0;channel< CHANNEL_NUM; channel++)
	  {
				LDC1614_set_Rp(channel,8.402);
				LDC1614_set_L(channel,10.02);
				LDC1614_set_C(channel,330);
				LDC1614_set_Q_factor(channel,48.03);
			
				// config SLEEP mode
				LDC1614_set_config(id, 0x2000);
			  int time = 1000;
	      while(time--);
			
				LDC1614_reset(id);
			
			  LDC1614_set_FIN_FREF_DIV(id, channel);
			
				calc_settling_time(channel);
				calc_conver_time(channel);
				LDC1614_set_LC_stabilize_time(id, channel);

				/*Set conversion interval time*/
				LDC1614_set_conversion_time(id, channel);

				/*Set driver current!*/
				LDC1614_set_driver_current(id, channel, sample_[id * 4 + channel]);	
				
				time = 1000;
	      while(time--);
		}

    /*single conversion*/
    LDC1614_set_mux_config(id, 0xc20d);
		
    /*start channel 0*/
    u16 config = 0x1601; // 1601
    LDC1614_set_config(id, config);
		
    return 0;
}


void ldc1614_iic_init(void)
{
	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOB_CLK_ENABLE();
	__HAL_RCC_GPIOC_CLK_ENABLE();
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.Pin = GPIO_PIN_6|GPIO_PIN_7;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF4_I2C1;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	/* Peripheral clock enable */
	__HAL_RCC_I2C1_CLK_ENABLE();
	GPIO_InitStruct.Pin = GPIO_PIN_10|GPIO_PIN_11;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF4_I2C2;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	/* Peripheral clock enable */
	__HAL_RCC_I2C2_CLK_ENABLE();
	GPIO_InitStruct.Pin = GPIO_PIN_9;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF4_I2C3;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = GPIO_PIN_8;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF4_I2C3;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	/* Peripheral clock enable */
	__HAL_RCC_I2C3_CLK_ENABLE();

  i2c_handle[0].Instance = I2C1;
  i2c_handle[0].Init.ClockSpeed = 100000;
  i2c_handle[0].Init.DutyCycle = I2C_DUTYCYCLE_2;
  i2c_handle[0].Init.OwnAddress1 = 0;
  i2c_handle[0].Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  i2c_handle[0].Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  i2c_handle[0].Init.OwnAddress2 = 0;
  i2c_handle[0].Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  i2c_handle[0].Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&i2c_handle[0]) != HAL_OK)
  {
		//print("iic1 init error!!!\n");
  }

  i2c_handle[1].Instance = I2C2;
  i2c_handle[1].Init.ClockSpeed = 50000;
  i2c_handle[1].Init.DutyCycle = I2C_DUTYCYCLE_2;
  i2c_handle[1].Init.OwnAddress1 = 0;
  i2c_handle[1].Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  i2c_handle[1].Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  i2c_handle[1].Init.OwnAddress2 = 0;
  i2c_handle[1].Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  i2c_handle[1].Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&i2c_handle[1]) != HAL_OK)
  {
		//print("iic2 init error!!!\n");
  }

  i2c_handle[2].Instance = I2C3;
  i2c_handle[2].Init.ClockSpeed = 50000;
  i2c_handle[2].Init.DutyCycle = I2C_DUTYCYCLE_2;
  i2c_handle[2].Init.OwnAddress1 = 0;
  i2c_handle[2].Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  i2c_handle[2].Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  i2c_handle[2].Init.OwnAddress2 = 0;
  i2c_handle[2].Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  i2c_handle[2].Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&i2c_handle[2]) != HAL_OK)
  {
		//print("iic3 init error!!!\n");
  }
}


void ldc1614_init()
{
	ldc1614_iic_init();
	int device_num = 3;
	for(int id=0; id<device_num;id++)
		LDC1614_mul_channel_config(id);
}

void ldc1614_single_init_device(unsigned char device_id){
	ldc1614_speic_reset(device_id);
	switch(device_id){
		case 0: i2c_handle[device_id].Instance = I2C1; break;
		case 1: i2c_handle[device_id].Instance = I2C2; break;
		case 2: i2c_handle[device_id].Instance = I2C3; break;
		default:break;
	}
  i2c_handle[device_id].Init.ClockSpeed = 50000;
  i2c_handle[device_id].Init.DutyCycle = I2C_DUTYCYCLE_2;
  i2c_handle[device_id].Init.OwnAddress1 = 0;
  i2c_handle[device_id].Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  i2c_handle[device_id].Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  i2c_handle[device_id].Init.OwnAddress2 = 0;
  i2c_handle[device_id].Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  i2c_handle[device_id].Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&i2c_handle[device_id]) != HAL_OK)
  {
		//print("iic1 init error!!!\n");
  }
	//LDC1614_mul_channel_config(device_id);
}

void ldc1614_speic_reset(unsigned char device_id){
//	i2c_handle[device_id].Instance->CR1= I2C_CR1_SWRST;  
//	i2c_handle[device_id].Instance->CR1= 0; 
//	HAL_I2C_DeInit(&i2c_handle[device_id]); 
	GPIO_InitTypeDef GPIO_InitStruct;
	if(device_id == 0){
		GPIO_InitStruct.Pin = GPIO_PIN_6|GPIO_PIN_7;
		GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
		GPIO_InitStruct.Pull = GPIO_PULLUP;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
		GPIO_InitStruct.Alternate = GPIO_AF4_I2C1;
		HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
		__I2C1_CLK_ENABLE();
		__I2C1_FORCE_RESET();
		__I2C1_RELEASE_RESET();
	}
	else if(device_id == 1){
		GPIO_InitStruct.Pin = GPIO_PIN_10|GPIO_PIN_11;
		GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
		GPIO_InitStruct.Pull = GPIO_PULLUP;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
		GPIO_InitStruct.Alternate = GPIO_AF4_I2C2;
		HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);	
		__I2C2_CLK_ENABLE();
		__I2C2_FORCE_RESET();
		__I2C2_RELEASE_RESET();	
	}
	else{
		GPIO_InitStruct.Pin = GPIO_PIN_9;
		GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
		GPIO_InitStruct.Pull = GPIO_PULLUP;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
		GPIO_InitStruct.Alternate = GPIO_AF4_I2C3;
		HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

		GPIO_InitStruct.Pin = GPIO_PIN_8;
		GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
		GPIO_InitStruct.Pull = GPIO_PULLUP;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
		GPIO_InitStruct.Alternate = GPIO_AF4_I2C3;
		HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);	
		__I2C3_CLK_ENABLE();
		__I2C3_FORCE_RESET();
		__I2C3_RELEASE_RESET();	
	}
}








