#include "key.h"
#include "stm32f4xx_hal.h"
#include "cmsis_os.h"
void key_init(Global_Inform_* global_inform){

	GPIO_InitTypeDef GPIO_InitStruct;

	struct CONTROL_* led_control_ = GetControlMethod();
	struct CONTROL_* key_input = GetInputEvent();
	
	
	for(unsigned int i = 0; i < PORT_NUM; i++){
		GPIO_InitStruct.Pin = key_input[i].Pin;
		GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
		GPIO_InitStruct.Pull = GPIO_PULLUP;
		HAL_GPIO_Init(key_input[i].GPIOx, &GPIO_InitStruct);	
		HAL_GPIO_WritePin(led_control_[i].GPIOx, led_control_[i].Pin, GPIO_PIN_RESET);
	}

	HAL_NVIC_SetPriority(EXTI2_IRQn, 12, 0);
	HAL_NVIC_EnableIRQ(EXTI2_IRQn);	
	
	HAL_NVIC_SetPriority(EXTI3_IRQn, 12, 0);
	HAL_NVIC_EnableIRQ(EXTI3_IRQn);
	
	HAL_NVIC_SetPriority(EXTI4_IRQn, 12, 0);
	HAL_NVIC_EnableIRQ(EXTI4_IRQn);
	
	HAL_NVIC_SetPriority(EXTI9_5_IRQn, 13, 0);
	HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);

	HAL_NVIC_SetPriority(EXTI15_10_IRQn, 11, 0);
	HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);	
	
	get_resert_status(global_inform);
}

void judge_key_status(Global_Inform_* global_inform){
	struct CONTROL_* led_control_ = GetControlMethod();
	struct CONTROL_* key_input = GetInputEvent();
	unsigned int status[PORT_NUM] = {0,0,0,0,0,0,0,0,0,0,0,0};
	unsigned int value = 0;
	for(unsigned int i = 0; i < PORT_NUM; i++){
		if(HAL_GPIO_ReadPin(key_input[i].GPIOx, key_input[i].Pin) == 0){
			status[i] = 1;
		}
		else{
			status[i] = 0;
		}		
	}
	HAL_Delay(20);
	for(unsigned int i = 0; i < PORT_NUM; i++){
		if(status[i] == 1){
			if(HAL_GPIO_ReadPin(key_input[i].GPIOx, key_input[i].Pin) == 0){
				value |= 1 << (i * 2);
				value &= ~(1 << (i * 2 + 1));
				HAL_GPIO_WritePin(led_control_[i].GPIOx, led_control_[i].Pin,GPIO_PIN_SET);
			}
			else{
				value |= 1 << (i * 2 + 1);
				value &= ~(1 << (i * 2));
				HAL_GPIO_WritePin(led_control_[i].GPIOx, led_control_[i].Pin,GPIO_PIN_RESET);
			}
		}
		else{
			value |= 1 << (i * 2 + 1);
			value &= ~(1 << (i * 2));
			HAL_GPIO_WritePin(led_control_[i].GPIOx, led_control_[i].Pin,GPIO_PIN_RESET);
		}		
	}
	xEventGroupSetBits(global_inform->dispatch_inform.xEventGroup, value);
}

void get_resert_status(Global_Inform_* global_inform){

	if(__HAL_RCC_GET_FLAG(RCC_FLAG_WWDGRST) != RESET){
		printf("wwdgrst reset\n\r");
	}
	else if(__HAL_RCC_GET_FLAG(RCC_FLAG_IWDGRST) != RESET){
		global_inform->dispatch_inform.concentrator_status = ONLINE;
		judge_key_status(global_inform);
		printf("iwdgrst reset\n\r");
	}
	else if(__HAL_RCC_GET_FLAG(RCC_FLAG_SFTRST) != RESET){   //������λ
		judge_key_status(global_inform);
		printf("softrst reset\n\r");
	}	
	else if(__HAL_RCC_GET_FLAG(RCC_FLAG_LPWRRST) != RESET){   // Low Power reset
		printf("lowpower reset\n\r");
	}	
	else if(__HAL_RCC_GET_FLAG(RCC_FLAG_BORRST) != RESET) {
		printf("borrst reset\n\r");
	}
	__HAL_RCC_CLEAR_RESET_FLAGS();
}
