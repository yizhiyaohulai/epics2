#include "normal_task.h"
#include "can.h"
#include "led.h"
#include "wwdg.h"
#include "w25q16.h"
#include <string.h>
#include "key.h"

#define PORT_NUM 12

#define NORMAL_TASK_TICKS 250

#define BOARD_SAVE_TICKS_NUM  9000
#define PORT_TEST_TICKS_NUM 120
#define PORT_INIT_TEST_TICKS_NUM 20
#define PORT_REGISTER_BOARD_NUM  4

static unsigned char led_refresh[PORT_NUM] = {0,0,0,0,0,0,0,0,0,0,0,0};
static unsigned char port_test_count[PORT_NUM] = {0,0,0,0,0,0,0,0,0,0,0,0};
static unsigned char port_init_test_count[PORT_NUM] = {0,0,0,0,0,0,0,0,0,0,0,0};

static unsigned char register_board_status = 0;
static unsigned char register_board_num = 0;

																																																							
void CallBackControlFunction(Global_Inform_* global_inform){
	struct CONTROL_* led_control_ = GetControlMethod();
	for(unsigned i = 0; i < PORT_NUM; i++ ){
		if(global_inform->dispatch_inform.normal_status[i] == 1){
			HAL_GPIO_WritePin(led_control_[i].GPIOx,led_control_[i].Pin,GPIO_PIN_SET);
			global_inform->dispatch_inform.led_status[i] = 1;
		}
		else{
			HAL_GPIO_WritePin(led_control_[i].GPIOx,led_control_[i].Pin,GPIO_PIN_RESET);
			global_inform->dispatch_inform.led_status[i] = 0;
		}
	}
}																					
																																			
void normal_task(void  * argument)
{
	Global_Inform_* global_inform = (Global_Inform_ *)argument;
	global_inform->dispatch_inform.cmd_status = CAN_IDLE;
	unsigned int i =0;
	unsigned int fault_status = 0;
	struct CONTROL_* led_control_ = GetControlMethod();
	while(1)
  {
		//-------------------------------------------------week dog-------------------------------------------------
		iwdg_refresh();
		//----------------------------------------------Control logic-----------------------------------------------		
		switch(global_inform->dispatch_inform.cmd_status){
			//------------------CAN_REGISTER_BOARD_START-----------//
			case CAN_REGISTER_BOARD_START:
					if(register_board_num > PORT_REGISTER_BOARD_NUM){
						if(register_board_status == 0){
							for(i = 0; i < PORT_NUM; i++){
								HAL_GPIO_WritePin(led_control_[i].GPIOx,led_control_[i].Pin,GPIO_PIN_RESET);
							}
							register_board_status = 1;
						}
						else{
							for(i = 0; i < PORT_NUM; i++){
								HAL_GPIO_WritePin(led_control_[i].GPIOx,led_control_[i].Pin,GPIO_PIN_SET);
							}
							register_board_status = 0;						
						}
						register_board_num = 0;
					}
					register_board_num++;
				break;
			case CAN_MODIFY_BOARD_START:
					if(register_board_num > PORT_REGISTER_BOARD_NUM){
						if(register_board_status == 0){
							for(i = 0; i < PORT_NUM; i++){
								HAL_GPIO_WritePin(led_control_[i].GPIOx,led_control_[i].Pin,GPIO_PIN_RESET);
							}
							register_board_status = 1;
						}
						else{
							for(i = 0; i < PORT_NUM; i++){
								HAL_GPIO_WritePin(led_control_[i].GPIOx,led_control_[i].Pin,GPIO_PIN_SET);
							}
							register_board_status = 0;						
						}
						register_board_num = 0;
					}
					register_board_num++;
				break;					
			case CAN_REGISTER_BOARD_ID:
				write_demarcate_value(global_inform);
				global_inform->TxMessage.Data[0] = CAN_REGISTER_FINISH;
				Can_MsgSleepSend(1);
				global_inform->dispatch_inform.cmd_status = CAN_IDLE;	
			  __disable_irq();
				HAL_NVIC_SystemReset();
				break;
			case CAN_MODIFY_BOARD_ID:
				write_demarcate_value(global_inform);
				global_inform->TxMessage.Data[0] = CAN_MODIFY_FINISH;
				global_inform->TxMessage.Data[1] = global_inform->demarcate_inform.board_id;
				Can_MsgSleepSend(2);
			  global_inform->dispatch_inform.cmd_status = CAN_IDLE;
				__disable_irq();
				HAL_NVIC_SystemReset();
				break;
			//-------------------Fault---------------------------//
			case CAN_PORT_IN_MALFUNCTION:                                                            //1
				fault_status = 0;
				for( i = 0; i < PORT_NUM; i++)
				{
					if(global_inform->dispatch_inform.malfunction_status[i] == 1){
							if(led_refresh[i] == 0){
								HAL_GPIO_WritePin(led_control_[i].GPIOx,led_control_[i].Pin,GPIO_PIN_RESET);
								global_inform->dispatch_inform.led_status[i] = 0;
								led_refresh[i] = 1;
							}
							else{
								HAL_GPIO_WritePin(led_control_[i].GPIOx,led_control_[i].Pin,GPIO_PIN_SET);
								global_inform->dispatch_inform.led_status[i] = 1;
								led_refresh[i] = 0;					
							}
						fault_status =1;	
					}	
				}
				if(fault_status == 0){
					CallBackControlFunction(global_inform);
					global_inform->dispatch_inform.cmd_status = CAN_PORT_NORMAL;
				}
				break;
			//-------------------------PORT INIT TEST----------------------//  5sec
			case CAN_PORT_IN_INIT_TEST:                                                             //2
				for(i = 0; i < PORT_NUM; i++ ){
					if(global_inform->dispatch_inform.init_test_status[i] == 1){
							if(led_refresh[i] == 0){
								HAL_GPIO_WritePin(led_control_[i].GPIOx,led_control_[i].Pin,GPIO_PIN_RESET);
								global_inform->dispatch_inform.led_status[i] = 0;
								led_refresh[i] = 1;
							}
							else{
								HAL_GPIO_WritePin(led_control_[i].GPIOx,led_control_[i].Pin,GPIO_PIN_SET);
								global_inform->dispatch_inform.led_status[i] = 1;
								led_refresh[i] = 0;					
							}
							port_init_test_count[i]++;
							if(port_init_test_count[i] >= PORT_INIT_TEST_TICKS_NUM){
									global_inform->dispatch_inform.init_test_status[i] = 0;
									port_init_test_count[i] =0;
									unsigned char switch_status = 0;
									for(int t = 0; t < PORT_NUM; t++)
										if(global_inform->dispatch_inform.init_test_status[i] == 1)
											switch_status = 1;
									if(switch_status == 0){
										CallBackControlFunction(global_inform);
										global_inform->dispatch_inform.cmd_status = CAN_PORT_NORMAL;
									}		
							}
					}
				}					
				break;
			//------------------------Port  test-------------------------//   
			case CAN_PORT_IN_TEST:                                                                  //3
				for(i = 0; i < PORT_NUM; i++ ){
					if(global_inform->dispatch_inform.test_status[i] == 1){
							if(led_refresh[i] == 0){
                HAL_GPIO_WritePin(led_control_[i].GPIOx,led_control_[i].Pin,GPIO_PIN_RESET);
								global_inform->dispatch_inform.led_status[i] = 0;
								led_refresh[i] = 1;
							}
							else{
								HAL_GPIO_WritePin(led_control_[i].GPIOx,led_control_[i].Pin,GPIO_PIN_SET);
								global_inform->dispatch_inform.led_status[i] = 1;
								led_refresh[i] = 0;					
							}
							port_test_count[i]++;
							if(port_test_count[i] >= PORT_TEST_TICKS_NUM){
								global_inform->dispatch_inform.test_status[i] = 0;
								port_test_count[i] =0;
								
								unsigned char switch_status = 0;
								for(int t = 0; t < PORT_NUM; t++)
										if(global_inform->dispatch_inform.test_status[i] == 1)
											switch_status = 1;
										
								if(switch_status == 0){
									CallBackControlFunction(global_inform);
									global_inform->dispatch_inform.cmd_status = CAN_PORT_NORMAL;	
								}
							}
					}
				}				
				break;
			case CAN_PORT_NORMAL: 
			  break;
			case CAN_IDLE:
				for( i = 0; i < PORT_NUM; i++)
				{
					if(global_inform->dispatch_inform.malfunction_status[i] == 1)
						HAL_GPIO_WritePin(led_control_[i].GPIOx,led_control_[i].Pin,GPIO_PIN_SET);
					else
						HAL_GPIO_WritePin(led_control_[i].GPIOx,led_control_[i].Pin,GPIO_PIN_RESET);
				}
				global_inform->dispatch_inform.cmd_status = CAN_PORT_NORMAL;
				break;
		}
		osDelay(250);
  }
}
