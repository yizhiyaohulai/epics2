#ifndef __M_MATH_H
#define __M_MATH_H

#define QueueSize      16
#define QueueFull      0  //fifo满置0
#define QueueOperateOk 2  
#define QueueStatusChange 3
#define QueueEmpty     1

struct FifoQueue
{
	unsigned int front;     //队列头
	unsigned int rear;      //队列尾   
	unsigned int count;     //队列计数
	unsigned int ave;
	unsigned int max;
	unsigned int min;
	unsigned int dat[QueueSize];
};

void QueueInit(struct FifoQueue *Queue);
unsigned int QueueOut(struct FifoQueue *Queue);
unsigned int QueueIn(struct FifoQueue *Queue, unsigned int sdat);
void  QueueClear(struct FifoQueue *Queue);
#endif
















