#ifndef __CAN_H_
#define __CAN_H_

#include "stm32f4xx_hal.h"
#include "cmsis_os.h"
#include "common.h"

void can_init(Global_Inform_* global_inform);
HAL_StatusTypeDef Can_MsgSend(unsigned char len);
void HAL_FilterConfig(unsigned int can_id);
HAL_StatusTypeDef Can_MsgSleepSend(uint8_t len);
HAL_StatusTypeDef Can_MsgSendIT(uint8_t len);
#endif






