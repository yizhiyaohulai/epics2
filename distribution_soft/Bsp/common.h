#ifndef __COMMON_H
#define __COMMON_H
#include "cmsis_os.h"
#include "stm32f4xx_hal.h"

#define NVIC_PriorityGrop_0  0

#define USART_PRE_TRANSFER  0
#define USART_FINISH_TRANSFER 1

#define ONLINE 1
#define OFFLINE 0

#define PORT_NUM 12

typedef enum {
  CAN_PORT_OUT_NON_EMPTY,
  CAN_PORT_IN_MALFUNCTION,
  CAN_PORT_IN_INIT_TEST,
  CAN_PORT_IN_TEST,
  CAN_PORT_OUT_LEDON,  // 4
  CAN_PORT_OUT_ENCODE00,
  CAN_PORT_OUT_ENCODE01,
  CAN_PORT_OUT_ENCODE02,
  CAN_PORT_OUT_ENCODE03,
  CAN_PORT_OUT_ENCODE04,
  CAN_PORT_OUT_ENCODE05,
  CAN_PORT_OUT_ENCODE06,
  CAN_PORT_OUT_ENCODE07,
  CAN_PORT_OUT_ENCODE08,
  CAN_PORT_OUT_ENCODE09,
  CAN_PORT_OUT_ENCODE10,
  CAN_PORT_OUT_CANID0,      // 16
  CAN_PORT_NORMAL,          //
  CAN_CONCENTRATOR_CAN_ID,  // 18
  CAN_BOARD_RESET,
  CAN_IDLE,
  CAN_BOARD_ONLINE_NOTIFY,
  CAN_REGISTER_BOARD_START,    // 0x15
  CAN_REGISTER_BOARD_ID,       // 0x16
  CAN_REGISTER_FINISH,         // 0x17
  CAN_MODIFY_BOARD_START,      // 0x18
  CAN_MODIFY_BOARD_ID,         // 0x19
  CAN_MODIFY_FINISH,           // 0x1a
  CAN_EXIT_RGISTER             // 0X1b
}CAN_COMMAND;

#define BIT_0 (1 << 0)
#define BIT_1 (1 << 1)
#define BIT_2 (1 << 2)
#define BIT_3 (1 << 3)
#define BIT_4 (1 << 4)
#define BIT_5 (1 << 5)
#define BIT_6 (1 << 6)
#define BIT_7 (1 << 7)
#define BIT_8 (1 << 8)
#define BIT_9 (1 << 9)
#define BIT_10 (1 << 10)
#define BIT_11 (1 << 11)
#define BIT_12 (1 << 12)
#define BIT_13 (1 << 13)
#define BIT_14 (1 << 14)
#define BIT_15 (1 << 15)
#define BIT_16 (1 << 16)
#define BIT_17 (1 << 17)
#define BIT_18 (1 << 18)
#define BIT_19 (1 << 19)
#define BIT_20 (1 << 20)
#define BIT_21 (1 << 21)
#define BIT_22 (1 << 22)
#define BIT_23 (1 << 23)

#define BIT_ALL (BIT_0 | BIT_1 | BIT_2 | BIT_3 | \
								 BIT_4 | BIT_5 | BIT_6 | BIT_7 | \
								 BIT_8 | BIT_9 | BIT_10 | BIT_11 | \
								 BIT_12 | BIT_13 | BIT_14 | BIT_15 | \
								 BIT_16 | BIT_17 | BIT_18 | BIT_19 | \
								 BIT_20 | BIT_21 | BIT_22 | BIT_23)

struct CONTROL_{
	GPIO_TypeDef* GPIOx;
	uint16_t Pin;
};	

typedef struct {
	unsigned int board_id;
	unsigned int can_id;
	unsigned int concentrate_can_id;
	char port_encode[12][40];
}Demarcate_Inform_;

typedef struct {
	QueueHandle_t usart_queue;
	QueueHandle_t can_queue;
	EventGroupHandle_t xEventGroup;
	unsigned char concentrator_status;
	unsigned char cmd_status;
	unsigned char malfunction_status[12];
	unsigned char register_board_id;
	unsigned char modify_can_id;
	unsigned char init_test_status[12];
	unsigned char test_status[12];
	unsigned char normal_status[12];
	unsigned char led_status[12];
}Dispatch_Inform_;


typedef struct {
	Demarcate_Inform_ demarcate_inform;
	Dispatch_Inform_  dispatch_inform;
	CanTxMsgTypeDef  TxMessage;
	CanRxMsgTypeDef  RxMessage;
}Global_Inform_;

struct CONTROL_* GetInputEvent(void);
struct CONTROL_* GetControlMethod(void);
void SystemClock_Config(void);
#endif

