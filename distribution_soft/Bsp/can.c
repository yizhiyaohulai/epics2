#include "can.h"

#define CAN_QUEUE_FIFO 1

CAN_HandleTypeDef hcan1;
CAN_FilterConfTypeDef  sFilterConfig;

//波特率:APB1/Prescaler/(1+2+1)== 125k

Global_Inform_* can_inform;

void can_init(Global_Inform_* global_inform){
	can_inform = global_inform;
	global_inform->dispatch_inform.can_queue  = xQueueCreate(CAN_QUEUE_FIFO, sizeof(unsigned char));
	
	__HAL_RCC_CAN1_CLK_ENABLE();
	__HAL_RCC_GPIOD_CLK_ENABLE();
	
// HAL_GPIO_WritePin(GPIOD, GPIO_PIN_0|GPIO_PIN_1, GPIO_PIN_RESET);
//
//	GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1;
//	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
//	GPIO_InitStruct.Pull = GPIO_PULLUP;
//	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
//	GPIO_InitStruct.Alternate = GPIO_AF9_CAN1;
//	HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);
  hcan1.Instance = CAN1;
  hcan1.Init.Prescaler = 4;
  hcan1.Init.Mode = CAN_MODE_NORMAL;
  hcan1.Init.SJW = CAN_SJW_1TQ;
  hcan1.Init.BS1 = CAN_BS1_2TQ;
  hcan1.Init.BS2 = CAN_BS2_1TQ;
 
  hcan1.Init.TTCM = DISABLE;
  hcan1.Init.ABOM = ENABLE;         // 自动离线管理
  hcan1.Init.AWUM = ENABLE;         // 自动唤醒模式
	hcan1.Init.NART = DISABLE;				//禁止报文自动重发                     //DISABLE的意思就是使能自动重传
  hcan1.Init.RFLM = DISABLE;        //接收FIFO锁定模式
  hcan1.Init.TXFP = DISABLE;        //发送FIFO优先级
  if (HAL_CAN_Init(&hcan1) != HAL_OK)
  {
		printf("can init failure\n\r");
  }
	
  sFilterConfig.FilterNumber = 5;
  sFilterConfig.FilterMode  =  CAN_FILTERMODE_IDMASK;
  sFilterConfig.FilterScale =  CAN_FILTERSCALE_16BIT;
  sFilterConfig.FilterIdHigh = 0x300 <<5;
  sFilterConfig.FilterIdLow = 0x200 <<5;
	
	sFilterConfig.FilterMaskIdHigh = (0x300 << 5)|0x10;
  sFilterConfig.FilterMaskIdLow = (0x300 << 5)|0x10;
  sFilterConfig.FilterFIFOAssignment = CAN_FILTER_FIFO0;
  sFilterConfig.FilterActivation = ENABLE;
  sFilterConfig.BankNumber = 14;
  HAL_CAN_ConfigFilter(&hcan1, &sFilterConfig);
	
	HAL_NVIC_SetPriority(CAN1_RX0_IRQn, 15, 0);
	HAL_NVIC_EnableIRQ(CAN1_RX0_IRQn);
	HAL_CAN_Receive_IT(&hcan1, CAN_FIFO0);
	
	hcan1.pTxMsg = &global_inform->TxMessage;
	hcan1.pRxMsg = &global_inform->RxMessage;
	
	
	hcan1.pTxMsg->StdId = global_inform->demarcate_inform.can_id; 
	hcan1.pTxMsg->RTR = CAN_RTR_DATA;
	hcan1.pTxMsg->IDE = CAN_ID_STD;
}

void HAL_FilterConfig(unsigned int can_id){

	__HAL_RCC_CAN1_CLK_DISABLE();
	HAL_CAN_DeInit(&hcan1);
	HAL_NVIC_DisableIRQ(CAN1_RX0_IRQn);
	
	__HAL_RCC_CAN1_CLK_ENABLE();
	HAL_CAN_MspDeInit(&hcan1);
	
	hcan1.Instance = CAN1;
  hcan1.Init.Prescaler = 4;
  hcan1.Init.Mode = CAN_MODE_NORMAL;
  hcan1.Init.SJW = CAN_SJW_1TQ;
  hcan1.Init.BS1 = CAN_BS1_2TQ;
  hcan1.Init.BS2 = CAN_BS2_1TQ;
 
  hcan1.Init.TTCM = DISABLE;
  hcan1.Init.ABOM = DISABLE;
  hcan1.Init.AWUM = DISABLE;
  //hcan1.Init.NART = DISABLE;
	hcan1.Init.NART=ENABLE;				//禁止报文自动传送
  hcan1.Init.RFLM = DISABLE;
  hcan1.Init.TXFP = DISABLE;
  if (HAL_CAN_Init(&hcan1) != HAL_OK)
  {
		printf("can init failure\n\r");
  }
	
	
	//printf("can id:%d\n\r",can_id);
	sFilterConfig.FilterNumber = 0;
  sFilterConfig.FilterMode  =  CAN_FILTERMODE_IDLIST;
  sFilterConfig.FilterScale =  CAN_FILTERSCALE_16BIT;
  sFilterConfig.FilterIdHigh = can_id << 5;
  sFilterConfig.FilterIdLow = 0;
	sFilterConfig.FilterMaskIdHigh = 0;
  sFilterConfig.FilterMaskIdLow = 0;
  sFilterConfig.FilterFIFOAssignment = CAN_FIFO0;
  sFilterConfig.FilterActivation = ENABLE;
  sFilterConfig.BankNumber = 14;
  HAL_CAN_ConfigFilter(&hcan1, &sFilterConfig);
	
	HAL_NVIC_SetPriority(CAN1_RX0_IRQn, 15, 0);
	HAL_NVIC_EnableIRQ(CAN1_RX0_IRQn);
	HAL_CAN_Receive_IT(&hcan1, CAN_FIFO0);
}

void HAL_CAN_RxCpltCallback(CAN_HandleTypeDef* hcan){
	if(hcan->Instance == CAN1){
		HAL_CAN_Receive_IT(&hcan1, CAN_FIFO0);
		//printf("cmd:%d\n\r",can_inform->RxMessage.Data[0]);
		printf("StdId:%x,cmd:%x\n\r",can_inform->RxMessage.StdId,can_inform->RxMessage.Data[0]);
		if(can_inform->RxMessage.StdId == can_inform->demarcate_inform.concentrate_can_id)
				can_inform->dispatch_inform.concentrator_status = ONLINE;
		
		if(can_inform->RxMessage.Data[0] == CAN_PORT_IN_MALFUNCTION || can_inform->RxMessage.Data[0] == CAN_PORT_IN_INIT_TEST || \
			 can_inform->RxMessage.Data[0] == CAN_PORT_IN_TEST){
				unsigned int cmd = can_inform->RxMessage.Data[0];
				can_inform->dispatch_inform.cmd_status = cmd;
				switch(cmd){
					case CAN_PORT_IN_MALFUNCTION:                                                                  //1
						if(can_inform->RxMessage.Data[3] == can_inform->demarcate_inform.board_id){
								unsigned int value= can_inform->RxMessage.Data[2];
								for(unsigned int i = 0; i < 8; i++)
									can_inform->dispatch_inform.malfunction_status[i] =  (value >> i) & 0x01;
								can_inform->dispatch_inform.malfunction_status[8] = can_inform->RxMessage.Data[1] &0x1;
								can_inform->dispatch_inform.malfunction_status[9] = can_inform->RxMessage.Data[1] &0x2;
								can_inform->dispatch_inform.malfunction_status[10] = can_inform->RxMessage.Data[1] &0x4;
								can_inform->dispatch_inform.malfunction_status[11] = can_inform->RxMessage.Data[1] &0x8;
						}
						break;
					case CAN_PORT_IN_INIT_TEST:                                                                    //2
						if(can_inform->RxMessage.Data[3] == can_inform->demarcate_inform.board_id){
							unsigned int value= can_inform->RxMessage.Data[2];
							for(unsigned int i = 0; i < 8; i++)
								can_inform->dispatch_inform.init_test_status[i] = (value >> i) & 0x01;
							can_inform->dispatch_inform.init_test_status[8] = can_inform->RxMessage.Data[1] &0x1;
							can_inform->dispatch_inform.init_test_status[9] = can_inform->RxMessage.Data[1] &0x2;
							can_inform->dispatch_inform.init_test_status[10] = can_inform->RxMessage.Data[1] &0x4;
							can_inform->dispatch_inform.init_test_status[11] = can_inform->RxMessage.Data[1] &0x8;						
						}
						break;
					case CAN_PORT_IN_TEST:                                                                        //3
						if(can_inform->RxMessage.Data[3] == can_inform->demarcate_inform.board_id){
							unsigned int value= can_inform->RxMessage.Data[2];
							for(unsigned int i = 0; i < 8; i++)
								can_inform->dispatch_inform.test_status[i] =  (value >> i) & 0x01;
							can_inform->dispatch_inform.test_status[8] = can_inform->RxMessage.Data[1] &0x1;
							can_inform->dispatch_inform.test_status[9] = can_inform->RxMessage.Data[1] &0x2;
							can_inform->dispatch_inform.test_status[10] = can_inform->RxMessage.Data[1] &0x4;
							can_inform->dispatch_inform.test_status[11] = can_inform->RxMessage.Data[1] &0x8;						
						}
						break;
					default:break; 
				}
		}
		else if(can_inform->RxMessage.Data[0] == CAN_REGISTER_BOARD_START || can_inform->RxMessage.Data[0] == CAN_MODIFY_BOARD_START || \
						can_inform->RxMessage.Data[0] == CAN_REGISTER_BOARD_ID || can_inform->RxMessage.Data[0] == CAN_MODIFY_BOARD_ID){
				unsigned short register_can_id  = can_inform->RxMessage.Data[1] * 255 + can_inform->RxMessage.Data[2];
				if(can_inform->RxMessage.Data[0] == CAN_REGISTER_BOARD_START || can_inform->RxMessage.Data[0] == CAN_MODIFY_BOARD_START ){
					if(register_can_id == can_inform->demarcate_inform.can_id)
						can_inform->dispatch_inform.cmd_status = can_inform->RxMessage.Data[0];
					else
						can_inform->dispatch_inform.cmd_status = CAN_IDLE;
				}
				else if(can_inform->RxMessage.Data[0] == CAN_REGISTER_BOARD_ID){
					if(register_can_id == can_inform->demarcate_inform.can_id){
						can_inform->dispatch_inform.cmd_status = CAN_REGISTER_BOARD_ID;
						can_inform->demarcate_inform.board_id = can_inform->RxMessage.Data[3];
					}
				} else if(can_inform->RxMessage.Data[0] == CAN_MODIFY_BOARD_ID) {
					if(register_can_id == can_inform->demarcate_inform.can_id){
						can_inform->dispatch_inform.cmd_status = CAN_MODIFY_BOARD_ID;
						can_inform->demarcate_inform.board_id = can_inform->RxMessage.Data[3];					
					}
				}
		}	 
		else if(can_inform->RxMessage.Data[0] == CAN_EXIT_RGISTER){
			can_inform->dispatch_inform.cmd_status = CAN_IDLE;
		}
		else if(can_inform->RxMessage.Data[0] == CAN_BOARD_RESET ||\
			can_inform->RxMessage.Data[0] == CAN_PORT_OUT_LEDON  || can_inform->RxMessage.Data[0] == CAN_CONCENTRATOR_CAN_ID){
			unsigned int cmd = can_inform->RxMessage.Data[0];
			BaseType_t xHigherPriorityTaskWoken = pdFALSE;
			xQueueSendFromISR(can_inform->dispatch_inform.can_queue, &cmd,&xHigherPriorityTaskWoken);
		}	
	}
}

HAL_StatusTypeDef Can_MsgSend(uint8_t len)
{
	unsigned int send_num = 0;
	HAL_StatusTypeDef retSTD;
	hcan1.pTxMsg->DLC = len;
	hcan1.pTxMsg->StdId = can_inform->demarcate_inform.can_id;
	while(1){
		retSTD= HAL_CAN_Transmit(&hcan1,200);
		if(retSTD == HAL_OK)
			break;
		send_num++;
		if(send_num>3)
			break;
	}
	return retSTD;  
}


HAL_StatusTypeDef Can_MsgSleepSend(uint8_t len)
{
	HAL_StatusTypeDef retSTD;
	hcan1.pTxMsg->DLC = len;
	hcan1.pTxMsg->StdId = can_inform->demarcate_inform.can_id;
	while(1){
		retSTD= HAL_CAN_Transmit(&hcan1,200);
		if(retSTD == HAL_OK)
			break;
		osDelay(100);
	}
	return retSTD;  
}

