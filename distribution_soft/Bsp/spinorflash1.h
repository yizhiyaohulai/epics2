#ifndef __SPINOR_FLASA_H
#define __SPINOR_FLASA_H

#include "common.h"

enum
{
	/* Command definitions (please see datasheet for more details) */

	/* RESET Operations */
	SPI_FLASH_INS_REN		  					= 0x66,	/* reset enable */
	SPI_FLASH_INS_RMEM		  				= 0x99,	/* reset memory */

	/* IDENTIFICATION Operations */
	SPI_FLASH_INS_RDID        			   = 0x9F,	/* read Identification */

	/* READ operations */
	SPI_FLASH_INS_READ        			   = 0x03,	/* read data bytes */
	SPI_FLASH_INS_FAST_READ   			   = 0x0B,	/* read data bytes at higher speed */

	/* WRITE operations */
	SPI_FLASH_INS_WREN        			   = 0x06,	/* write enable */
	SPI_FLASH_INS_WRDI        			   = 0x04,	/* write disable */

	/* REGISTER operations */
	SPI_FLASH_INS_RDSR      				= 0x05,	/* read status register */
	SPI_FLASH_INS_WRSR      				= 0x01,	/* write status register */
	SPI_FLASH_INS_RDLR                  = 0xE8, /* read lock register */
	SPI_FLASH_INS_CMD_WRLR              = 0xE5, /* write lock register */
	SPI_FLASH_INS_RFSR     			 		= 0x70,	/* read flag status register */
	SPI_FLASH_INS_CLFSR     				= 0x50,	/* clear flag status register */
	SPI_FLASH_INS_RDNVCR    				= 0xB5,	/* read non volatile configuration register */
	SPI_FLASH_INS_WRNVCR    				= 0xB1,	/* write non volatile configuration register */
	SPI_FLASH_INS_RDVCR     				= 0x85,	/* read volatile configuration register */
	SPI_FLASH_INS_WRVCR     				= 0x81,	/* write volatile configuration register */
	SPI_FLASH_INS_RDVECR    				= 0x65,	/* read volatile enhanced configuration register */
	SPI_FLASH_INS_WRVECR    				= 0x61,	/* write volatile enhanced configuration register */

	/* PROGRAM operations */
	SPI_FLASH_INS_PP          			   = 0x02,	/* PAGE PROGRAM */

	/* ERASE Operations */
	SPI_FLASH_INS_SSE								= 0x20,	/* SUBSECTOR ERASE */
	SPI_FLASH_INS_SE								= 0xD8,	/* SECTOR ERASE */
	SPI_FLASH_INS_BE								= 0xC7,	/* BULK ERASE */
	SPI_FLASH_INS_PER         			      = 0x7A,	/* program Erase Resume */
	SPI_FLASH_INS_PES         			      = 0x75,	/* program Erase Suspend */

	/* OTP operations */
	SPI_FLASH_INS_RDOTP							= 0x4B, /* read OTP array */
	SPI_FLASH_INS_PROTP							= 0x42, /* program OTP array */

	/* DEEP POWER-DOWN operation */
	SPI_FLASH_INS_ENTERDPD						= 0xB9, /* enter deep power-down */
	SPI_FLASH_INS_RELEASEDPD					= 0xA8  /* release deep power-down */
};

/* Status Register Definitions */
enum
{
	SPI_FLASH_SRWD	   = 0x80,				/* status Register Write Protect */
	SPI_FLASH_BP3		= 0x40,				/* block Protect Bit3 */
	SPI_FLASH_TB		= 0x20,				/* top/Bottom bit */
	SPI_FLASH_BP2		= 0x10,				/* block Protect Bit2 */
	SPI_FLASH_BP1		= 0x08,				/* block Protect Bit1 */
	SPI_FLASH_BP0		= 0x04,				/* block Protect Bit0 */
	SPI_FLASH_WEL		= 0x02,				/* write Enable Latch */
	SPI_FLASH_WIP		= 0x01				/* write/Program/Erase in progress bit */
};



void spinorflash_init(void);
void spi_norflash_exit(void);
unsigned int FlashReadDevId(void);
unsigned int read_demarcate_value(Global_Inform_* global_inform);
unsigned int write_demarcate_value(Global_Inform_* global_inform);
unsigned int write_demarcate_256_value(Global_Inform_* global_inform);
unsigned int write_somedemarcate_value(Global_Inform_* global_inform);
#endif

