#include "flash.h"
#include "stm32f4xx_hal.h"
#include "string.h"
#include "stdio.h"
#include "stdlib.h"
FLASH_EraseInitTypeDef EraseInitStruct;

//can id 
//0 canid 7
//1 canid 6
//2 canid 5
//3 canid 4
//4 canid 3
//5 canid 2
//6 canid 1
//7 canid 0

//concentrator can id 
//8 canid 7
//9 canid 6
//10 canid 5
//11 canid 4
//12 canid 3
//13 canid 2
//14 canid 1
//15 canid 0

//threshold_status
//(20-23) (24-27) (28-31) (32-35) (36-39) (40-43) (44-47) (48-51) (52-55) (56-59) (60-63) (64-67)

//board id 
//68-71


//passwd
// (90-97)

//already save passwd
//98

//port 0 edcode
//100 _ 
//port 1 edcode
//140 _ 
//port 2 edcode
//180 _ 
//port 3 edcode
//220 _ 
//port 4 edcode
//260 _ 
//port 5 edcode
//300 _ 
//port 6 edcode
//340 _ 
//port 7 edcode
//380 _ 
//port 8 edcode
//420_ 
//port 9 edcode
//460 _ 
//port 10 edcode
//500 _ 
//port 11 edcode
//540_ 579

void flash_write(Global_Inform_* global_inform){
	uint32_t PageError = 0;
	
	char buffer[580];
	unsigned int i = 0;
//can id 
	buffer[0] = 0;
	buffer[1] = 0;
	buffer[2] = 0;
	buffer[3] = 0;
	buffer[4] = (global_inform->demarcate_inform.can_id >> 24) & 0xff;
	buffer[5] = (global_inform->demarcate_inform.can_id >> 16) & 0xff;
	buffer[6] = (global_inform->demarcate_inform.can_id >>8) & 0xff;
	buffer[7] = global_inform->demarcate_inform.can_id & 0xff;
//concentrator can id 	
	buffer[8] = 0;
	buffer[9] = 0;
	buffer[10] = 0;
	buffer[11] = 0;
	buffer[12] = (global_inform->demarcate_inform.concentrate_can_id >> 24) & 0xff;
	buffer[13] = (global_inform->demarcate_inform.concentrate_can_id >> 16) & 0xff;
	buffer[14] = (global_inform->demarcate_inform.concentrate_can_id >>8) & 0xff;
	buffer[15] = global_inform->demarcate_inform.concentrate_can_id & 0xff;	
	//board id 
	buffer[68] = (global_inform->demarcate_inform.board_id >> 24) & 0xff;
	buffer[69] = (global_inform->demarcate_inform.board_id >> 16) & 0xff;
	buffer[70] = (global_inform->demarcate_inform.board_id >> 8) & 0xff;
	buffer[71] = global_inform->demarcate_inform.board_id & 0xff;
	
	//passwd
	memcpy(buffer + 90 ,global_inform->demarcate_inform.passwd, 8);

	//already save passwd
	buffer[98] = global_inform->demarcate_inform.already_save_passwd;
	
	//port num edcode	
	unsigned int offset = 100;
	for(i = 0; i < 12; i++){
		for(unsigned int j = 0; j < 40; j++)
			buffer[offset + j ] = global_inform->demarcate_inform.port_encode[i][j];
	  offset += 40;
	}
	HAL_FLASH_Unlock();
	EraseInitStruct.TypeErase     = FLASH_TYPEERASE_SECTORS;
	EraseInitStruct.VoltageRange  = FLASH_VOLTAGE_RANGE_3;
  EraseInitStruct.Sector        = FLASH_SECTOR_11;
  EraseInitStruct.NbSectors     = 1;
	HAL_FLASHEx_Erase(&EraseInitStruct,&PageError);
	
	uint32_t address =  ((uint32_t)0x080E0000);
	for(i =0; i < 580;i++)
		HAL_FLASH_Program(TYPEPROGRAM_BYTE, address+i, buffer[i]);
	HAL_FLASH_Lock();
}

unsigned int flash_read(Global_Inform_* global_inform){

	uint32_t address =  ((uint32_t)0x080E0000);
	char buffer[580];
	
	for(int i =0 ; i< 580; i++)
		buffer[i] = *(__IO uint32_t*)(address + i);
	
	global_inform->demarcate_inform.can_id = (buffer[4] <<24) | (buffer[5] << 16) | (buffer[6] << 8) | buffer[7];
	global_inform->demarcate_inform.concentrate_can_id = (buffer[12] << 24) | (buffer[13] << 16) | (buffer[14] << 8) | buffer[15];
	

	global_inform->demarcate_inform.board_id = (buffer[68] << 24) | (buffer[69] << 16) | (buffer[70] << 8) | buffer[71];
	
	memcpy(global_inform->demarcate_inform.passwd ,buffer + 90, 8);
	
	global_inform->demarcate_inform.already_save_passwd = buffer[98];
	
	//port num edcode		
	unsigned int offset = 100;
	for(int i = 0; i < 12; i++){
		memcpy(global_inform->demarcate_inform.port_encode[i], buffer + offset, 40);
		offset += 40;
	}
	return 0;
}

