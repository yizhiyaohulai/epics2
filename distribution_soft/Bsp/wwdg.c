#include "wwdg.h"

//WWDG_HandleTypeDef hwwdg;
IWDG_HandleTypeDef hiwdg;
//T= 4096*Prescaler*(T[5:0]+1)/Fpclk;  = 4096*8/8*
//Fpclk APB1 clock
//T[5:0] low 6bit
#if 0
void wwdg_init(void)
{
	__WWDG_CLK_ENABLE();
	hwwdg.Instance = WWDG;
	hwwdg.Init.Prescaler = WWDG_PRESCALER_8;
  hwwdg.Init.Window = 0x70;
  hwwdg.Init.Counter = 0x70; 
	hwwdg.Init.EWIMode = WWDG_EWI_ENABLE;
	HAL_WWDG_Init(&hwwdg);
	HAL_NVIC_SetPriority(WWDG_IRQn, 12, 0);
	HAL_NVIC_EnableIRQ(WWDG_IRQn);
	__HAL_WWDG_ENABLE_IT(&hwwdg,WWDG_IT_EWI);
	__HAL_WWDG_ENABLE(&hwwdg);
	HAL_WWDG_Refresh(&hwwdg);
	printf("wwdg init\n\r");
}

void HAL_WWDG_EarlyWakeupCallback(WWDG_HandleTypeDef* ahwwdg)
{
	HAL_WWDG_Refresh(ahwwdg); 
	printf("WWDG\n\r");
}
#endif


void iwdg_init(void){
  hiwdg.Instance = IWDG;
  hiwdg.Init.Prescaler = IWDG_PRESCALER_256;
  hiwdg.Init.Reload = 0x7ff;
	HAL_IWDG_Init(&hiwdg);
}

void iwdg_refresh(){
	HAL_IWDG_Refresh(&hiwdg);
}
