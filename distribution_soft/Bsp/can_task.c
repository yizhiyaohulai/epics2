#include "can_task.h"
#include "stdio.h"
#include "can.h"
#include "w25q16.h"
#include "cmsis_armcc.h"
#include "string.h"

#define CAN_HOLDKEYTIME 10000

static unsigned char can_status=0;

void can_task(void  * argument)
{
	Global_Inform_* global_inform = (Global_Inform_ *)argument;
	unsigned int  i =0;
	unsigned char value = 0;
	unsigned int concentrate_can_id = 0;
	unsigned char filter_finish_status = 0;
  while(1)
  {
		if (xQueueReceive(global_inform->dispatch_inform.can_queue, &can_status, CAN_HOLDKEYTIME/portTICK_RATE_MS) == pdPASS){
			switch(can_status){
				case CAN_PORT_OUT_LEDON:
					for(i = 0; i < 8; i++){
							if(global_inform->dispatch_inform.led_status[i])
								value |= (1 << i);
					}
					global_inform->TxMessage.Data[0] = CAN_PORT_OUT_LEDON;
					global_inform->TxMessage.Data[2] = value;
					value = 0;
					for(i = 8; i < 12; i++){
							if(global_inform->dispatch_inform.led_status[i])
								value |= (1 <<(i - 8));
					}
					global_inform->TxMessage.Data[1] = value;
					Can_MsgSleepSend(3);
					break;
				case CAN_CONCENTRATOR_CAN_ID:
				  concentrate_can_id = global_inform->RxMessage.Data[4] | (global_inform->RxMessage.Data[3] << 8) | \
															(global_inform->RxMessage.Data[2] << 16) | (global_inform->RxMessage.Data[1] << 24);
					if(filter_finish_status == 0){
						//HAL_FilterConfig(global_inform->demarcate_inform.concentrate_can_id);
						if(concentrate_can_id != global_inform->demarcate_inform.concentrate_can_id){
							global_inform->demarcate_inform.concentrate_can_id = concentrate_can_id;
							//vTaskSuspendAll();
							write_demarcate_value(global_inform);
							//HAL_Delay(100);
							//xTaskResumeAll();
						}
						filter_finish_status = 1;
						global_inform->dispatch_inform.concentrator_status = ONLINE;
					}
					break;
				case CAN_BOARD_RESET:
					printf("system reset-----------------------------------------------------\n\r");
					__disable_irq();
					HAL_NVIC_SystemReset();
					break;
				default :break;
			}
		}
  }
}




