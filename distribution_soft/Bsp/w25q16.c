#include "w25q16.h"
#include "stm32f4xx_hal.h"
#include "string.h"
#include "stdlib.h"

SPI_HandleTypeDef hspi1;

void w25qxx_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;
	
	__GPIOA_CLK_ENABLE();
	__SPI1_CLK_ENABLE();
	
	//CS	
	GPIO_InitStruct.Pin			=GPIO_PIN_4;
	GPIO_InitStruct.Mode		=GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Speed  	=GPIO_SPEED_FAST;
	GPIO_InitStruct.Pull 		=GPIO_PULLUP;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	
  hspi1.Instance 								= SPI1;
  hspi1.Init.Mode 							= SPI_MODE_MASTER;
  hspi1.Init.Direction 					= SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize 					= SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity 				= SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase 					= SPI_PHASE_1EDGE;
  hspi1.Init.NSS 								= SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler 	= SPI_BAUDRATEPRESCALER_2;
  hspi1.Init.FirstBit 					= SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode 						= SPI_TIMODE_DISABLED;
  hspi1.Init.CRCCalculation 		= SPI_CRCCALCULATION_DISABLED;
  hspi1.Init.CRCPolynomial 			= 7;
  HAL_SPI_Init(&hspi1);
	
	__HAL_SPI_ENABLE(&hspi1);
	
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_SET);	
}

unsigned short W25q64_ReadID(void)
{
	unsigned char Data1[4] 	= {0x90,0x00,0x00,0x00};
	unsigned char Data2[2]	= {0x00,0x00};
	unsigned char Rxdata[2];
	
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_RESET);
	
	HAL_SPI_Transmit(&hspi1,Data1,4,100);
	HAL_SPI_TransmitReceive(&hspi1,Data2,Rxdata,2,100);
	
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_SET);
	
	return Rxdata[1]|Rxdata[0];
}


//写一页256字节内
void SPI_Flash_WritePage(unsigned char* pBuffer, unsigned int WriteAddr,unsigned short NumByteToWrite)
{
	unsigned char Data1[4];
	unsigned int i=0;
	SPI_FLASH_Write_Enable();
	
	Data1[0]=W25X_CMD_PageProgram;
	Data1[1]=(unsigned char)((WriteAddr)>>16);
	Data1[2]=(unsigned char)((WriteAddr)>>8);
	Data1[3]=(unsigned char)WriteAddr;
	
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_RESET);
	HAL_SPI_Transmit(&hspi1,Data1,4,100);
	
	for(i=0;i<NumByteToWrite;i++)
	{
		HAL_SPI_Transmit(&hspi1,&pBuffer[i],1,100);
	}

	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_SET);
	SPI_Flash_Wait_Busy();
}


//无校验写SPI FLASH 
//必须确保所写的地址范围内的数据全部为0XFF,否则在非0XFF处写入的数据将失败
//具有自动换页功能
//在指定地址开始写入指定长度的数据，但是要确保地址不越界
//pBuffer:数据存储器
//WriteAddr:开始写入的地址(24bit)
//NumByteToWrite:要写入的字节数(最大65535)
//每页256个字节
void SPI_Flash_Write_NoCheck(unsigned char* pBuffer,int WriteAddr,unsigned short NumByteToWrite)   
{ 			 		 
	unsigned short YeSengYu;	   
	YeSengYu = 256-WriteAddr%256; //单节剩余的字节数
	if(NumByteToWrite <= YeSengYu)
		YeSengYu = NumByteToWrite;	//???256???
	while(1)
	{	   
		SPI_Flash_WritePage(pBuffer,WriteAddr,YeSengYu);
		if(NumByteToWrite == YeSengYu)
			break;//?????
	 	else 		// NumByteToWrite > YeSengYu
		{
			pBuffer += YeSengYu;
			WriteAddr += YeSengYu;
			NumByteToWrite -= YeSengYu;			  //???????????
			if(NumByteToWrite > 256)
				YeSengYu = 256; 								//一次可以写入256个字节
			else
				YeSengYu = NumByteToWrite; 	  	//不够256个字节了
		}
	}    
} 


//读
void SPI_Flash_ReadBuffer(unsigned char * pBuffer, unsigned int ReadAddr, unsigned short NumByteToRead)
{
	unsigned char Data1[4];
//	unsigned char i=0;
	
	Data1[0]=W25X_CMD_ReadData;
	Data1[1]=(unsigned char)((ReadAddr)>>16);
	Data1[2]=(unsigned char)((ReadAddr)>>8);
	Data1[3]=(unsigned char)ReadAddr;
	
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_RESET);
	
	HAL_SPI_Transmit(&hspi1,Data1,4,100);
	
	HAL_SPI_Receive(&hspi1,pBuffer,NumByteToRead,100);
	
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_SET);
}

//写SPI FLASH  
//在指定地址开始写入指定长度的数据
//该函数带擦除操作!
//pBuffer:数据存储区
//WriteAddr:开始写入的地址(24bit)
//NumByteToWrite:要写入的字节数(最大65535)  		   

void SPI_Flash_Write(unsigned char* pBuffer,unsigned int WriteAddr,unsigned short NumByteToWrite)   
{ 
	unsigned int secpos;
	unsigned short secoff;
	unsigned short secremain;	   
 	unsigned short i;    

	unsigned char SPI_FLASH_BUF[4096];
	
	secpos = WriteAddr / 4096;						//扇区地址0~2047 for w25x64
	secoff = WriteAddr % 4096;						//扇区地址偏移
	secremain = 4096 - secoff;						//扇区剩余空间  

	if(NumByteToWrite <= secremain)
		secremain = NumByteToWrite;					//不大于4096个字节
	while(1) 
	{	
		SPI_Flash_ReadBuffer(SPI_FLASH_BUF,secpos*4096,4096);				//读出整个扇区的内容
		for(i=0;i<secremain;i++)																		//校验数据
		{
			if(SPI_FLASH_BUF[secoff+i] != 0XFF)
				break;//需要擦除 	  
		}
		if(i<secremain)//需要擦除 
		{
			SPI_Flash_Erase_Sector(secpos);				//擦除这个扇区
			for(i=0;i<secremain;i++)	   					//复制
			{
				SPI_FLASH_BUF[i+secoff] = pBuffer[i];	  
			}
			SPI_Flash_Write_NoCheck(SPI_FLASH_BUF,secpos*4096,4096);//写入整个扇区
		}
		else 
			SPI_Flash_Write_NoCheck(pBuffer,WriteAddr,secremain);//写已经擦除了的，直接写入扇区剩余空间			   
		if(NumByteToWrite == secremain)
			break;//写入结束了
		else//写入未结束
		{
			secpos++;							//扇区地址增1
			secoff=0;							//偏移位置为0 	 
			pBuffer += secremain; //指针偏移
			WriteAddr += secremain;	//写地址偏移
		  NumByteToWrite -= secremain;//字节数递减
			if(NumByteToWrite>4096)
				secremain = 4096;			//下一个扇区还是写不完
			else 
				secremain = NumByteToWrite;					//下一个扇区可以写完了
		}	 
	}
}


//擦除一个扇区
//Dst_Addr:扇区地址0~511  for w25x16
//Dst_Addr:扇区地址0~2047 for w25x64
//擦除一个扇区大概150ms
void SPI_Flash_Erase_Sector(unsigned int Dst_Addr)
{
	unsigned char TxData[4];
	Dst_Addr*=4096;
	
	TxData[0]=W25X_CMD_SectorErase;
	TxData[1]=(unsigned char)((Dst_Addr)>>16);
	TxData[2]=(unsigned char)((Dst_Addr)>>8);
	TxData[3]=(unsigned char)Dst_Addr;
	
	SPI_FLASH_Write_Enable();

	SPI_Flash_Wait_Busy();
	
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_RESET);
	
	HAL_SPI_Transmit(&hspi1,TxData,4,100);
	
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_SET);
	
	SPI_Flash_Wait_Busy();
}


void SPI_Flash_Erase_Chip(void)   
{ 
	unsigned char TXData[1]={W25X_CMD_ChipErase};
	
  SPI_FLASH_Write_Enable();           //SET WEL 
  SPI_Flash_Wait_Busy();   
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_RESET); 
	
	HAL_SPI_Transmit(&hspi1,TXData,1,100); 
	
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_SET);  	      
	SPI_Flash_Wait_Busy();   				   	
} 



//等待空闲
void SPI_Flash_Wait_Busy(void)   
{   
	while ((SPI_Flash_ReadSR()&0x01)==0x01);  // ??BUSY???
}

//读取SPI_FLASH的状态寄存器
//BIT7  6   5   4   3   2   1   0
//SPR   RV  TB BP2 BP1 BP0 WEL BUSY
//SPR:默认0,状态寄存器保护位,配合WP使用
//TB,BP2,BP1,BP0:FLASH区域写保护位置
//WEL:写使能锁定
//BUSY:忙标记位(1,忙;0,空闲)
//默认:0x00
unsigned char SPI_Flash_ReadSR(void)   
{  
	unsigned char TxData[1]={W25X_CMD_ReadStatusReg1};
	unsigned char TxData1[1]={0xff};
	
	unsigned char RxData[1];
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_RESET);
	
	HAL_SPI_Transmit(&hspi1,TxData,1,100);
	HAL_SPI_TransmitReceive(&hspi1,TxData1,RxData,1,100);
	
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_SET);    
	return RxData[0];   
}

//写SPI_FLASH状态寄存器
void SPI_FLASH_Write_SR(unsigned char sr)   
{ 
	unsigned char TXData[2]={W25X_CMD_WriteStatusReg,sr};  
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_RESET);
	HAL_SPI_Transmit(&hspi1,TXData,2,100);                 			
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_SET);     	      
}


//SPI_FLASH写使能	
//将WEL置位
void SPI_FLASH_Write_Enable(void)   
{
	unsigned char TXData[1]={W25X_CMD_WriteEnable};
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_RESET);                           
  HAL_SPI_Transmit(&hspi1,TXData,1,100);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_SET);    	      
}


//SPI_FLASH写禁止	
//将WEL清零 
void SPI_FLASH_Write_Disable(void)   
{ 
	unsigned char TXData[1]={W25X_CMD_WriteDisable};
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_RESET); 
	HAL_SPI_Transmit(&hspi1,TXData,1,100);   
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_SET);     	      
} 

//进入掉电模式
void SPI_Flash_PowerDown(void)
{
	unsigned char data[1]={W25X_Power_Down};
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_RESET);
	
	HAL_SPI_Transmit(&hspi1,data,1,100);
	
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_SET);
}

//唤醒
void SPI_Flash_WAKEUP(void)
{
	unsigned char data[1]={W25X_WakeUp};
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_RESET);
	
	HAL_SPI_Transmit(&hspi1,data,1,100);
	
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_SET);
}



//can id 
//0 canid 7
//1 canid 6
//2 canid 5
//3 canid 4
//4 canid 3
//5 canid 2
//6 canid 1
//7 canid 0

//concentrator can id 
//8 canid 7
//9 canid 6
//10 canid 5
//11 canid 4
//12 canid 3
//13 canid 2
//14 canid 1
//15 canid 0

//threshold_status
//(20-23) (24-27) (28-31) (32-35) (36-39) (40-43) (44-47) (48-51) (52-55) (56-59) (60-63) (64-67)

//board id 
//68-71


//passwd
// (90-97)

//already save passwd
//98

//port 0 edcode
//100 _ 
//port 1 edcode
//140 _ 
//port 2 edcode
//180 _ 
//port 3 edcode
//220 _ 
//port 4 edcode
//260 _ 
//port 5 edcode
//300 _ 
//port 6 edcode
//340 _ 
//port 7 edcode
//380 _ 
//port 8 edcode
//420_ 
//port 9 edcode
//460 _ 
//port 10 edcode
//500 _ 
//port 11 edcode
//540_ 579

void read_demarcate_value(Global_Inform_* global_inform){
	static unsigned char buffer[1024];
	SPI_Flash_ReadBuffer(buffer,0,1024);
	global_inform->demarcate_inform.can_id = (buffer[4] <<24) | (buffer[5] << 16) | (buffer[6] << 8) | buffer[7];
	global_inform->demarcate_inform.concentrate_can_id = (buffer[12] << 24) | (buffer[13] << 16) | (buffer[14] << 8) | buffer[15];

	global_inform->demarcate_inform.board_id = (buffer[68] << 24) | (buffer[69] << 16) | (buffer[70] << 8) | buffer[71];
	
	//port num edcode		
	unsigned int offset = 100;
	for(int i = 0; i < 12; i++){
		memcpy(global_inform->demarcate_inform.port_encode[i], buffer + offset, 40);
		offset += 40;
	}
}

void write_demarcate_value(Global_Inform_* global_inform)
{
	static unsigned char buffer[1024];
	unsigned int i = 0;
//can id 
	buffer[0] = 0;
	buffer[1] = 0;
	buffer[2] = 0;
	buffer[3] = 0;
	buffer[4] = (global_inform->demarcate_inform.can_id >> 24) & 0xff;
	buffer[5] = (global_inform->demarcate_inform.can_id >> 16) & 0xff;
	buffer[6] = (global_inform->demarcate_inform.can_id >>8) & 0xff;
	buffer[7] = global_inform->demarcate_inform.can_id & 0xff;
//concentrator can id 	
	buffer[8] = 0;
	buffer[9] = 0;
	buffer[10] = 0;
	buffer[11] = 0;
	buffer[12] = (global_inform->demarcate_inform.concentrate_can_id >> 24) & 0xff;
	buffer[13] = (global_inform->demarcate_inform.concentrate_can_id >> 16) & 0xff;
	buffer[14] = (global_inform->demarcate_inform.concentrate_can_id >>8) & 0xff;
	buffer[15] = global_inform->demarcate_inform.concentrate_can_id & 0xff;	
	//board id 
	buffer[68] = (global_inform->demarcate_inform.board_id >> 24) & 0xff;
	buffer[69] = (global_inform->demarcate_inform.board_id >> 16) & 0xff;
	buffer[70] = (global_inform->demarcate_inform.board_id >> 8) & 0xff;
	buffer[71] = global_inform->demarcate_inform.board_id & 0xff;
	
	//port num edcode	
	unsigned int offset = 100;
	for(i = 0; i < 12; i++){
		for(unsigned int j = 0; j < 40; j++)
			buffer[offset + j ] = global_inform->demarcate_inform.port_encode[i][j];
	  offset += 40;
	}
	
	SPI_Flash_Erase_Sector(0);
	SPI_Flash_Write_NoCheck(buffer,0,1024);
}


