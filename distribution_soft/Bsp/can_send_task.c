#include "can_send_task.h"
#include "can.h"
#include "common.h"

const TickType_t xTicksToWait = 600000 / portTICK_PERIOD_MS;

void can_send_task(void  * argument)
{
	Global_Inform_* global_inform = (Global_Inform_ *)argument;
	EventBits_t uxBits;
  while(1)
  {
	  uxBits = xEventGroupWaitBits(global_inform->dispatch_inform.xEventGroup, \
												BIT_ALL, \
												pdTRUE , \
												pdFALSE, \
												xTicksToWait);
		for(signed i = 0; i < 24; i += 2){
			unsigned char status = uxBits & 0x03;
			if(status == 0x01){
				global_inform->dispatch_inform.normal_status[i / 2] = 1;
				//printf("index:%d,value:%d\n\r", i / 2, 1);
			}
			else if(status == 0x02){
				global_inform->dispatch_inform.normal_status[i / 2] = 0;
				//printf("index:%d,value:%d\n\r", i / 2, 0);
			}
			uxBits = uxBits >> 2;	
		}
		
		//upload port information
		unsigned int value = 0;
		for(unsigned int i = 0; i < 8; i++){
			if(global_inform->dispatch_inform.normal_status[i])
				value |= (1 << i);
		}
		global_inform->TxMessage.Data[0] = CAN_PORT_OUT_NON_EMPTY;
		global_inform->TxMessage.Data[2] = value;
		value = 0;
		for(unsigned int i = 8; i < 12; i++){
			if(global_inform->dispatch_inform.normal_status[i])
				value |= (1 <<(i - 8));
		}
		global_inform->TxMessage.Data[1] = value;
		global_inform->TxMessage.Data[3] = global_inform->demarcate_inform.board_id;
		Can_MsgSleepSend(4);	
  }
}

