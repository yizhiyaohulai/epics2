#!/home/manager/source/imx6/epics/concentrator_fs/bin/linux-arm/asApp

< /home/manager/source/imx6/epics/concentrator_fs/iocBoot/iocas/envPaths

cd "${TOP}"

dbLoadDatabase "/home/manager/source/imx6/epics/concentrator_fs/dbd/as.dbd"
as_registerRecordDeviceDriver(pdbbase)
concentratorConfigure("CAN0","cluster_id=01",96,12)

dbLoadTemplate("/home/manager/source/imx6/epics/concentrator_fs/db/user.substitutions","station_id=01,cluster_id=01,PORT=CAN0,ADDR=0,TIMEOUT=1,TSE=-2,SCAN=I/O Intr,FIFO=5")

cd "${TOP}/iocBoot/${IOC}"
save_restoreSet_Debug(0)
save_restoreSet_IncompleteSetsOk(1)

set_requestfile_path(std, "../../db/dbTest.db")
set_pass0_restoreFile("auto_test.sav")
set_pass1_restoreFile("auto_test.sav")
save_restoreSet_NumSeqFiles(3)
save_restoreSet_SeqPeriodInSeconds(600)
save_restoreSet_RetrySeconds(60)
save_restoreSet_CAReconnect(1)
save_restoreSet_CallbackTimeout(-1)

set_savefile_path("/home/manager/source/imx6/epics/concentrator_fs/iocBoot/iocas/autosave_data")
set_requestfile_path("/home/manager/source/imx6/epics/concentrator_fs/iocBoot/iocas", "")

epicsThreadSleep(0.1)
iocInit
create_monitor_set("auto_test.req", 5, "station_id=01,cluster_id=01")
