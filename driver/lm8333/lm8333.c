#include <linux/module.h>
#include <linux/irq.h>
#include <linux/i2c.h>
#include <linux/sched.h>
#include <linux/kthread.h>
#include <linux/interrupt.h>
#include <linux/delay.h> 
#include <linux/input.h>
#include <linux/of_gpio.h>
#include <linux/kernel.h>
#include <linux/slab.h>

#define LM8333_FIFO_READ		0x20
#define LM8333_DEBOUNCE			0x22
#define LM8333_READ_INT			0xD0
#define LM8333_ACTIVE			0xE4
#define LM8333_READ_ERROR		0xF0
 
#define LM8333_KEYPAD_IRQ		(1 << 0)
#define LM8333_ERROR_IRQ		(1 << 3)
 
#define LM8333_ERROR_KEYOVR		0x04
#define LM8333_ERROR_FIFOOVR		0x40
 
#define LM8333_FIFO_TRANSFER_SIZE	16
 
#define LM8333_ROW_SHIFT	8
#define LM8333_NUM_ROWS		2

#define LM8333_READ_RETRIES 2

struct lm8333 {
	struct i2c_client *client;
	struct input_dev *input;
	unsigned short keycodes[LM8333_NUM_ROWS * LM8333_ROW_SHIFT];
};

/* The accessors try twice because the first access may be needed for wakeup */
#define LM8333_READ_RETRIES 2

int lm8333_read8(struct lm8333 *lm8333, u8 cmd)
{
	int retries = 0, ret;

	do {
		ret = i2c_smbus_read_byte_data(lm8333->client, cmd);
	} while (ret < 0 && retries++ < LM8333_READ_RETRIES);

	return ret;
}

int lm8333_write8(struct lm8333 *lm8333, u8 cmd, u8 val)
{
	int retries = 0, ret;

	do {
		ret = i2c_smbus_write_byte_data(lm8333->client, cmd, val);
	} while (ret < 0 && retries++ < LM8333_READ_RETRIES);

	return ret;
}

int lm8333_read_block(struct lm8333 *lm8333, u8 cmd, u8 len, u8 *buf)
{
	int retries = 0, ret;

	do {
		ret = i2c_smbus_read_i2c_block_data(lm8333->client,
						    cmd, len, buf);
	} while (ret < 0 && retries++ < LM8333_READ_RETRIES);

	return ret;
}


//static void lm8333_

static void lm8333_key_handler(struct lm8333 *lm8333)
{
	struct input_dev *input = lm8333->input;
	u8 keys[LM8333_FIFO_TRANSFER_SIZE];
	u8 code, pressed;
	int i, ret;

	ret = lm8333_read_block(lm8333, LM8333_FIFO_READ,
				LM8333_FIFO_TRANSFER_SIZE, keys);
	if (ret != LM8333_FIFO_TRANSFER_SIZE) {
		dev_err(&lm8333->client->dev,
			"Error %d while reading FIFO\n", ret);
		return;
	}

	for (i = 0; i < LM8333_FIFO_TRANSFER_SIZE && keys[i]; i++) {
		pressed = keys[i] & 0x80;
		code = keys[i] & 0x7f;

		printk("i:%d,pressed:%d,code:%d\n\r",i,pressed,code);
		input_event(input, EV_MSC, MSC_SCAN, code);
		input_report_key(input, lm8333->keycodes[code], pressed);
	}

	input_sync(input);
}

static irqreturn_t lm8333_irq_thread(int irq, void *data)
{
	struct lm8333 *lm8333 = data;
	u8 status = lm8333_read8(lm8333, LM8333_READ_INT);

	if (!status){
		//return IRQ_NONE;
		return IRQ_HANDLED;
	}
		
	if (status & LM8333_ERROR_IRQ) {
		printk("LM8333_ERROR_IRQ\n\r");
		u8 err = lm8333_read8(lm8333, LM8333_READ_ERROR);

		if (err & (LM8333_ERROR_KEYOVR | LM8333_ERROR_FIFOOVR)) {
			u8 dummy[LM8333_FIFO_TRANSFER_SIZE];

			lm8333_read_block(lm8333, LM8333_FIFO_READ,
					LM8333_FIFO_TRANSFER_SIZE, dummy);
		}
		dev_err(&lm8333->client->dev, "Got error %02x\n", err);
	}

	if (status & LM8333_KEYPAD_IRQ){
		//printk("LM8333_KEYPAD_IRQ\n\r");
		lm8333_key_handler(lm8333);
	}
		
	return IRQ_HANDLED;
}


static int lm8333_irq_request(struct lm8333 *lm8333)
{
	int ret;
	struct i2c_client *client = lm8333->client;

	ret = devm_request_threaded_irq(&client->dev, client->irq, NULL,
					  lm8333_irq_thread,
					  IRQF_TRIGGER_LOW | IRQF_ONESHOT,
					  "lm8333", lm8333);
	if (ret < 0)
		dev_err(&client->dev, "Failed to register interrupt\n");

	return ret;
}

/* wake up controller by an falling edge of interrupt gpio.  */
static int lm8333_wake_up_device(struct i2c_client *client)
{
	struct device_node *np = client->dev.of_node;
	int gpio;
	int ret;

	if (!np)
		return -ENODEV;

	gpio = of_get_named_gpio(np, "wakeup-gpios", 0);
	if (!gpio_is_valid(gpio))
		return -ENODEV;

	ret = gpio_request(gpio, "lm8333_irq");
	if (ret < 0) {
		dev_err(&client->dev,
			"request gpio failed, cannot wake up controller: %d\n",
			ret);
		return ret;
	}

	/* wake up controller via an falling edge on IRQ gpio. */
	gpio_direction_output(gpio, 0);
	gpio_set_value(gpio, 1);

	/* controller should be waken up, return irq.  */
	gpio_direction_input(gpio);
	gpio_free(gpio);

	return 0;
}


static int lm8333_probe(struct i2c_client *client,
			 const struct i2c_device_id *id)
{
	printk("===============keypad lm8333_probe ==============\n");

	struct lm8333 *lm8333;
	struct input_dev *input;
	int err;

	int debounce_time = 30;
	int active_time = 500;

   unsigned short keycodes[16] = {KEY_1,KEY_2,KEY_3,KEY_4,KEY_5,KEY_UP,KEY_INSERT,KEY_ENTER,\
   						   KEY_6,KEY_7,KEY_8,KEY_9,KEY_0,KEY_DOWN,KEY_EXIT,KEY_DELETE};	

	printk(KERN_INFO, "chip found @ 0x%02x (%s)\n",
		client->addr << 1, client->adapter->name);

   lm8333 = kzalloc(sizeof(*lm8333), GFP_KERNEL);
   input = input_allocate_device();
   if (!lm8333 || !input) {
	   err = -ENOMEM;
	   goto free_mem;
   }

   lm8333->client = client;
   lm8333->input = input;

   input->name = client->name;
   input->dev.parent = &client->dev;
   input->id.bustype = BUS_I2C;

   input_set_capability(input, EV_MSC, MSC_SCAN);
						   
   memcpy(lm8333->keycodes,keycodes,LM8333_NUM_ROWS * LM8333_ROW_SHIFT * sizeof(unsigned short));


   if (debounce_time) {
	   err = lm8333_write8(lm8333, LM8333_DEBOUNCE,
				   debounce_time / 3);
	   if (err)
		   dev_warn(&client->dev, "Unable to set debounce time\n");
   }

   if (active_time) {
	   err = lm8333_write8(lm8333, LM8333_ACTIVE,
				   active_time / 3);
	   if (err)
		   dev_warn(&client->dev, "Unable to set active time\n");
   }

   /* controller may be in sleep, wake it up. */
   err = lm8333_wake_up_device(client);
   if (err) {
	   dev_err(&client->dev, "Failed to wake up the controller\n");
	   return err;
   }

   input_set_drvdata(input, lm8333);

   err  = lm8333_irq_request(lm8333);
   if (err)
		return err;

   err = input_register_device(input);
   if (err)
		goto free_mem;

   i2c_set_clientdata(client, lm8333);
   return 0;
free_mem:
   input_free_device(input);
   kfree(lm8333);
   return err;
}

 static int lm8333_remove(struct i2c_client *client)
 {
	 struct lm8333 *lm8333 = i2c_get_clientdata(client);
	 devm_free_irq(&client->dev, client->irq, lm8333);
	 input_unregister_device(lm8333->input);
	 kfree(lm8333);
	 return 0;
 }

static const struct of_device_id lm8333_of_id[] = {
 { .compatible = "ti,lm8333", },
 { },
};

static const struct i2c_device_id lm8333_id[] = {
	{"lm8333", 0},
	{},
};
	
MODULE_DEVICE_TABLE(i2c, lm8333_id);

static struct i2c_driver lm8333_i2c_driver = {
	.driver = {
		.owner = THIS_MODULE,
		.name = "lm8333",
		.of_match_table = of_match_ptr(lm8333_of_id),
	},
	.probe = lm8333_probe,
	.remove = lm8333_remove,
	.id_table = lm8333_id,
};

static int __init lm8333_init(void)
{
	return i2c_add_driver(&lm8333_i2c_driver);
}

static void __exit lm8333_exit(void)
{
	i2c_del_driver(&lm8333_i2c_driver);
}

MODULE_AUTHOR("Hu jun bao");
MODULE_DESCRIPTION("Keyboard driver");
MODULE_LICENSE("GPL");
module_init(lm8333_init);
module_exit(lm8333_exit);
