#include <linux/init.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/err.h>
#include <linux/errno.h>
#include <linux/mutex.h>
#include <linux/spi/spi.h>
#include <linux/spi/spidev.h>
#include <linux/of.h>
#include <linux/of_device.h>
#include <linux/of_gpio.h>
#include <linux/fb.h>
#include <linux/uaccess.h>
#include <linux/ioctl.h>
#include <linux/list.h>
#include <linux/slab.h>
#include <linux/compat.h>
#include <linux/dma-mapping.h>
#include <linux/timer.h>


#define SCREEN_X_SIZE 128
#define SCREEN_Y_SIZE 128

struct spidev_data {
	spinlock_t		spi_lock;
	struct spi_device	*spi;
	struct list_head	device_entry;
	struct mutex		buf_lock;
	unsigned		users;
	u8			*tx_buffer;
	u8			*rx_buffer;
	u32			speed_hz;
};

struct sharp_lcd_par {
	struct spidev_data* spidev;
	struct fb_info *info;
};

struct timer_list mytimer;

static int pwn_gpio;
static int status = 0;
static unsigned bufsiz = 4096;
module_param(bufsiz, uint, S_IRUGO);
MODULE_PARM_DESC(bufsiz, "data bytes in biggest supported SPI message");

static ssize_t 
spidev_sync(struct spidev_data *spidev, struct spi_message *message)
{
	DECLARE_COMPLETION_ONSTACK(done);
	int status;
	struct spi_device *spi;

	spin_lock_irq(&spidev->spi_lock);
	spi = spidev->spi;
	spin_unlock_irq(&spidev->spi_lock);

	if (spi == NULL)
		status = -ESHUTDOWN;
	else
		status = spi_sync(spi, message);

	if (status == 0)
		status = message->actual_length;

	return status;
}

static inline ssize_t 
spidev_sync_write(struct spidev_data *spidev, size_t len)
{
	struct spi_transfer	t = {
			.tx_buf		= spidev->tx_buffer,
			.len		= len,
			.speed_hz	= spidev->speed_hz,
		};
	struct spi_message	m;
	spi_message_init(&m);
	spi_message_add_tail(&t, &m);
	return spidev_sync(spidev, &m);
}


static u8 observer(u8 data){
	u8 val =0;
	u8 i = 0;
	for(i = 0; i < 8; i++){
		u8 temp = (data >> i) & 0x01;
		val |= temp << (7-i);
	}
	return val;
}

static void sharp_lcd_fb_update_display(struct sharp_lcd_par* par){
//	u32* p = (u32 *)par->info->screen_base;
	u8* vmem = par->info->screen_base;
	int y,x;
	int i,j;
	int offset = 0;

	unsigned char * array = kzalloc(par->info->var.xres * par->info->var.yres / 8, GFP_KERNEL);
	
	for(y= 0; y < par->info->var.yres; y++){
		for(x=0; x< par->info->var.xres; x++){
				unsigned int color = (vmem[y*512 +x*4] << 24)|(vmem[y*512 +x*4+1]<<16) | (vmem[y*512 +x*4+2]<< 8) | (vmem[y*512 +x*4+3]);
				//unsigned int val =p[y *fbi->var.xres + x];
				unsigned int pos = (16 * y) + (x / 8);
				unsigned int offset = x % 8;
				unsigned int val = ~(1 << (7- offset));
				array[pos] &= val;
				array[pos] |= (color > 0 ? 0 : 1) << (7- offset);
			}
	}

	
	for (i = 1; i < par->info->var.yres + 1; i++ )
	{
			par->spidev->tx_buffer[offset] = 0x80;
			offset++;
			par->spidev->tx_buffer[offset] = observer(i);
			offset++;
			
			for(j = 0; j < par->info->var.xres / 8; j++){
				par->spidev->tx_buffer[offset] = array[j + (i -1)*(par->info->var.xres / 8)];
				offset++;
			}
	}
	par->spidev->tx_buffer[offset] = 0xff;
	offset++;
	par->spidev->tx_buffer[offset] = 0xff;
	offset++;
	kfree(array);
	spidev_sync_write(par->spidev,offset);
	printk("sharp_lcd_fb_update_display\n\r");
}


static void sharp_lcd_fb_deferred_io(struct fb_info *info,
				struct list_head *pagelist)
{
	struct sharp_lcd_par *par;
	par = info->par;
	sharp_lcd_fb_update_display(par);
	gpio_set_value(pwn_gpio, status);
	 if(status)
		status = 0;
	else
		status = 1;
}

static struct fb_deferred_io sharp_lcd_fb_defio = {
	.delay			= HZ/60,
	.deferred_io	= sharp_lcd_fb_deferred_io,
};

struct fb_ops sharp_lcd_fops = {
	.owner			= THIS_MODULE,
	.fb_read		= fb_sys_read,
	.fb_write		= fb_sys_write,
	.fb_fillrect	= sys_fillrect,
	.fb_copyarea	= sys_copyarea,
	.fb_imageblit	= sys_imageblit,
};

static const struct of_device_id sharp_lcd_dt_ids[] = {
	{ .compatible = "sharp,ls013b7dh03" },
	{ /* sentinel */ }
};

MODULE_DEVICE_TABLE(of, sharp_lcd_dt_ids);

static int sharp_lcd_probe(struct spi_device *spi)
{
	struct spidev_data	*spidev;
	int status;
	struct fb_info *fbi;
	struct sharp_lcd_par *par;
	u32 vmem_size;
//	u8 *vmem;
    u8 *v_addr;
    u32 p_addr;

	/*
	 * spidev should never be referenced in DT without a specific
	 * compatible string, it is a Linux implementation thing
	 * rather than a description of the hardware.
	 */
	if (spi->dev.of_node && !of_match_device(sharp_lcd_dt_ids, &spi->dev)) {
		dev_err(&spi->dev, "buggy DT: spidev listed directly in DT\n");
		WARN_ON(spi->dev.of_node &&
			!of_match_device(sharp_lcd_dt_ids, &spi->dev));
	}

	if(!spi)	
		return -ENOMEM;

	pwn_gpio = of_get_named_gpio(spi->dev.of_node, "sharp-gpios", 0);
	printk("sharp-gpios:%d\n\r",pwn_gpio);
	if (!gpio_is_valid(pwn_gpio)) {
		dev_warn(&spi->dev, "no sharp-gpios pin available");
		return -EINVAL;
	}
	
	status = devm_gpio_request_one(&spi->dev, pwn_gpio, GPIOF_OUT_INIT_HIGH,
					"pwn_gpio");
	if (status < 0) {
		dev_warn(&spi->dev, "request of pwn_gpio failed");
		return status;
	}

	gpio_direction_output(pwn_gpio, 1);
	gpio_set_value(pwn_gpio, 1);


	vmem_size = SCREEN_X_SIZE * SCREEN_Y_SIZE * 4;

#if 0
	vmem = (void *)__get_free_pages(GFP_KERNEL | __GFP_ZERO,
					get_order(vmem_size));
	if (!vmem) {
		dev_err(&spidev->spi->dev, "Couldn't allocate graphical memory.\n");
		status = -ENOMEM;
		goto fb_alloc_error;
	}
#endif

	v_addr = dma_alloc_coherent(NULL, SCREEN_X_SIZE*SCREEN_Y_SIZE*4, &p_addr, GFP_KERNEL);
	if(v_addr == NULL) {
		goto fb_alloc_error;
		dev_err(&spi->dev, "DMA alloc fail\n");
	}

	fbi = framebuffer_alloc(sizeof(struct sharp_lcd_par), NULL);

	/* Allocate driver data */
	spidev = kzalloc(sizeof(*spidev), GFP_KERNEL);
	if (!spidev)
		return -ENOMEM;
	
	if (!spidev->tx_buffer) {
		printk("spi dev tx buffer alloc\n\r");
		spidev->tx_buffer = kmalloc(bufsiz, GFP_KERNEL);
		if (!spidev->tx_buffer) {
			dev_dbg(&spidev->spi->dev, "open/ENOMEM\n");
			status = -ENOMEM;
			return status;
		}
	}
	spidev->spi = spi;

	/* Initialize the driver data */
	par = fbi->par;
	par->info = fbi;
	par->spidev = spidev;

	spin_lock_init(&spidev->spi_lock);
	mutex_init(&spidev->buf_lock);

	spidev->speed_hz = 1100000;

	spi->max_speed_hz = 1100000;
	spi->mode = SPI_MODE_0 | SPI_CS_HIGH;
	spi->bits_per_word = 8;
	status = spi_setup(spi);
	//if (status < 0)
	//	dev_dbg(&spidev->spi->dev, "set spi dev failure\n");
	
	printk("%s:name=%s,bus_num=%d,cs=%d,mode=%d,speed=%d\n",__func__,spi->modalias, spi->master->bus_num, spi->chip_select, spi->mode, spi->max_speed_hz);

	fbi->var.xres = SCREEN_X_SIZE;
	fbi->var.yres = SCREEN_Y_SIZE;
	fbi->var.xres_virtual = SCREEN_X_SIZE;
	fbi->var.yres_virtual = SCREEN_Y_SIZE;
	fbi->var.bits_per_pixel = 32; 
	fbi->var.red.offset = 16;
	fbi->var.red.length = 8;
	fbi->var.green.offset = 8;
	fbi->var.green.length = 8;
	fbi->var.blue.offset = 0;
	fbi->var.blue.length = 8;

	//fbi->screen_base = (u8 __force __iomem *)vmem;
	fbi->screen_base = v_addr;
	fbi->screen_size = SCREEN_X_SIZE*SCREEN_Y_SIZE*4;
	//fbi->fix.smem_start = __pa(vmem);
	fbi->fix.smem_start = p_addr;
	fbi->fix.smem_len = vmem_size;
	fbi->fix.type = FB_TYPE_PACKED_PIXELS;
	fbi->fix.visual = FB_VISUAL_TRUECOLOR;
	fbi->fix.line_length = SCREEN_X_SIZE * 4;

	fbi->fbops = &sharp_lcd_fops;
	fbi->fbdefio = &sharp_lcd_fb_defio;

	fb_deferred_io_init(fbi);

	spi_set_drvdata(spi, fbi);

	status = register_framebuffer(fbi); 
	if (status) {
		dev_err(&spi->dev, "Couldn't register the framebuffer\n");
		//goto panel_init_error;
	}

	return 0;
bl_init_error:
	unregister_framebuffer(fbi);

fb_alloc_error:
	framebuffer_release(fbi);
	return status;
}

static int sharp_lcd_remove(struct spi_device *spi)
{
	struct sharp_lcd_par *par;
	struct fb_info *fbi = spi_get_drvdata(spi);
	unregister_framebuffer(fbi);
	//__free_pages(__va(fbi->fix.smem_start), get_order(fbi->fix.smem_len));
	dma_free_coherent(NULL, fbi->screen_size, fbi->screen_base, fbi->fix.smem_start);
	par = fbi->par;
	kfree(par->spidev);
	framebuffer_release(fbi);
	devm_gpio_free(&spi->dev,pwn_gpio);
	del_timer(&mytimer);
	return 0;
}

static struct spi_driver sharp_lcd_driver = {
	.driver = {
		.name	= "sharp_lcd",
		.of_match_table = sharp_lcd_dt_ids,
	},
	.probe	= sharp_lcd_probe,
	.remove	= sharp_lcd_remove,
};

static int __init sharp_lcd_init(void)
{
	return spi_register_driver(&sharp_lcd_driver);
}

static void __exit sharp_lcd_exit(void)
{
	spi_unregister_driver(&sharp_lcd_driver);
}

module_init(sharp_lcd_init);
module_exit(sharp_lcd_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("hujunbao");
MODULE_DESCRIPTION("sharp lcd driver");
