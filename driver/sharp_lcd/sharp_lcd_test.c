#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <linux/fb.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <stdlib.h>
#include <string.h>

void main(char argc, char** argv){

	int fbfd = 0;
	struct fb_var_screeninfo vinfo;
	struct fb_fix_screeninfo finfo;
	long int screensize = 0;
	char *fbp = 0;
	int location = 0;

	// Open the file for reading and writing
	fbfd = open("/dev/fb2", O_RDWR);
	if (fbfd < 0) {
	    printf("Error: cannot open framebuffer device.\n");
	    exit(1);
	}
	printf("The framebuffer device was opened successfully. fbfd=%x\n", fbfd);

	/* Get fixed screen information */
	if (ioctl(fbfd, FBIOGET_FSCREENINFO, &finfo)) {
			printf("Error reading fixed information.\n");
			exit(2);
	}

	/* Get variable screen information */
	if (ioctl(fbfd, FBIOGET_VSCREENINFO, &vinfo)) {
			printf("Error reading variable information.\n");
			exit(3);
	}
	
	/* show these information*/
	printf("vinfo.xres=%d\n",vinfo.xres);
	printf("vinfo.yres=%d\n",vinfo.yres);
	printf("vinfo.bits_per_bits=%d\n",vinfo.bits_per_pixel);
	printf("vinfo.xoffset=%d\n",vinfo.xoffset);
	printf("vinfo.yoffset=%d\n",vinfo.yoffset);
	printf("finfo.line_length=%d\n",finfo.line_length);
	
	screensize = vinfo.xres * vinfo.yres * vinfo.bits_per_pixel / 8;;
	printf("the screensize is %d\n", screensize );
	// Map the device to memory
	fbp = (char *)mmap(0, screensize, PROT_READ | PROT_WRITE, MAP_SHARED,
	                   fbfd, 0);
	if ((int)fbp == -1) {
	    printf("Error: failed to map framebuffer device to memory.\n");
	    exit(4);
	}
	printf("The framebuffer device was mapped to memory successfully.\n");

	int offset = 0;

	for(int y = 0; y < vinfo.yres; y++)
		for(int x = 0; x < vinfo.xres; x++)
		{
			location = (x + vinfo.xoffset) * (vinfo.bits_per_pixel/8) + (y + vinfo.yoffset) * finfo.line_length;
			*(fbp + location) 	  = 0x00;
			*(fbp + location + 1) = 0x00;
			*(fbp + location + 2) = offset/256;
			*(fbp + location + 3) = offset%256;
			offset++;
		}

	sleep(1);
	munmap(fbp, screensize);
	close(fbfd);
}



