#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/input.h>
#include <linux/init.h>
#include <linux/errno.h>
#include <linux/delay.h>
#include <linux/miscdevice.h>
#include <linux/platform_device.h>
#include <asm/uaccess.h>
#include <asm/irq.h>
#include <asm/io.h>
#include <asm/uaccess.h>
#include <linux/uaccess.h>
#include <linux/clk.h>
#include <linux/interrupt.h>
#include <linux/irq.h>
#include <linux/sched.h>

#include <linux/gpio.h>

#define IMX_GPIO_NR(bank, nr)	(((bank) - 1) * 32 + (nr))

#define SABRESD_Optical_DATA0 	IMX_GPIO_NR(1, 15)   //sd2_data0
#define SABRESD_Optical_DATA1 	IMX_GPIO_NR(1, 14)   //sd2_data1
#define SABRESD_Optical_DATA2	IMX_GPIO_NR(1, 10)   //sd2_clk
#define SABRESD_Optical_DATA3 	IMX_GPIO_NR(1, 13)   //sd2_data2
#define SABRESD_Optical_RESET 	IMX_GPIO_NR(1, 12)   //sd2_data3
#define SABRESD_Optical_ERROR 	IMX_GPIO_NR(1, 11)   //sd2_cmd
#define SABRESD_Optical_READY	IMX_GPIO_NR(6, 11)   //NANDF_CS0

//gpio_set_value
#define Optical_Reset_Cmd 		0xFFFF00
#define Optical_Control_Cmd 	0xFFFF01

static long Optical_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
	int data;
	switch(cmd){
		case Optical_Reset_Cmd: 
			__gpio_set_value(SABRESD_Optical_RESET, arg);
		break;
		case Optical_Control_Cmd: 
			data = arg - 1;
			__gpio_set_value(SABRESD_Optical_RESET, 1);
			__gpio_set_value(SABRESD_Optical_DATA0, data & 0x01);
			__gpio_set_value(SABRESD_Optical_DATA1, data & 0x02);
			__gpio_set_value(SABRESD_Optical_DATA2, data & 0x04);
			__gpio_set_value(SABRESD_Optical_DATA3, data & 0x08);
		break;
		default: break;
	}
	return 0;
}

static ssize_t Optical_read(struct file *filp, char *buffer,
		size_t count, loff_t *ppos)
{
	unsigned int str[1];
	int ready = 0;
	int error = 0;
	error = gpio_get_value(SABRESD_Optical_ERROR);  //high: error low: right
	ready = gpio_get_value(SABRESD_Optical_READY);  //high: wait  low: resume

	str[0] = error | ready;

	return copy_to_user(buffer,str,sizeof(str));
}

static struct file_operations OpticalSwitch_fops = {
	owner:	THIS_MODULE,
	read:	Optical_read,	
	unlocked_ioctl:	Optical_ioctl,
};

static struct miscdevice OpticalSwitch_misc ={
	.minor	= MISC_DYNAMIC_MINOR,
	.name	= "opticalswitch",
	.fops	= &OpticalSwitch_fops,
};

int __init OpticalSwitch_Init(void)
{
	int status;
	status=misc_register(&OpticalSwitch_misc);
	if(status<0)
		printk("can 't creat misc device\n\r");

	gpio_request(SABRESD_Optical_DATA0, "Optical_Data0");
	gpio_direction_output(SABRESD_Optical_DATA0,0);	
	gpio_request(SABRESD_Optical_DATA1, "Optical_Data1");
	gpio_direction_output(SABRESD_Optical_DATA1,0);	
	gpio_request(SABRESD_Optical_DATA2, "Optical_Data2");
	gpio_direction_output(SABRESD_Optical_DATA2,0);	
	gpio_request(SABRESD_Optical_DATA3, "Optical_Data3");
	gpio_direction_output(SABRESD_Optical_DATA3,0);	

	gpio_request(SABRESD_Optical_RESET, "Optical_Reset");
	gpio_direction_output(SABRESD_Optical_RESET,1);	


	gpio_request(SABRESD_Optical_ERROR, "Optical_Error");
	gpio_direction_input(SABRESD_Optical_ERROR);
	gpio_request(SABRESD_Optical_READY, "Optical_Ready");
	gpio_direction_input(SABRESD_Optical_READY);	
	
	return status;
}
static void __exit OpticalSwitch_Exit(void)
{
	gpio_free(SABRESD_Optical_DATA0);
	gpio_free(SABRESD_Optical_DATA1);
	gpio_free(SABRESD_Optical_DATA2);
	gpio_free(SABRESD_Optical_DATA3);

	gpio_free(SABRESD_Optical_RESET);

	gpio_free(SABRESD_Optical_ERROR);
	gpio_free(SABRESD_Optical_READY);

	misc_deregister(&OpticalSwitch_misc);	
}

module_init(OpticalSwitch_Init);
module_exit(OpticalSwitch_Exit);

MODULE_AUTHOR("hujunbao");
MODULE_DESCRIPTION("Switch Optical Driver");
MODULE_LICENSE("GPL");




